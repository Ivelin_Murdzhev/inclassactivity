package com.company;

import java.util.ArrayList;
import java.util.List;

public class ForumPost {

    String author;
    String text;
    int upVotes;
    List<String> replies = new ArrayList<>();

    public ForumPost(String author, String text, int upVotes) {
        this.author = author;
        this.text = text;
        this.upVotes = upVotes;
    }

    public ForumPost(String author, String text) {
        this(author, text, 0);
    }

    public void format() {
        System.out.println(getInfo());
        getElements();
    }

    private void getElements() {
        for (String reply : replies) {
            System.out.println(reply);
        }
    }

    private String getInfo() {
        return String.format("%s / by %s, %d votes. %n", this.text, this.author, this.upVotes);
    }
}
