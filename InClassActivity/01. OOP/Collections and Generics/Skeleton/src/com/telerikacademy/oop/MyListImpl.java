package com.telerikacademy.oop;

import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.IntStream;

public class MyListImpl<T> implements MyList<T> {

    /**
     * Default initial capacity.
     */
    private static final int DEFAULT_CAPACITY = 4;

    /**
     * The size of the ArrayList (the number of elements it contains).
     */
    int size;

    /**
     * The array buffer into which the elements of the ArrayList are stored.
     */
    private T[] elements;

    public MyListImpl(int initialCapacity) {

        this.size = 0;
        elements = (T[]) new Object[initialCapacity];
    }

    public MyListImpl() {
        this(DEFAULT_CAPACITY);
    }

    @Override
    public int size() {

        return this.size;
    }

    @Override
    public int capacity() {

        return this.elements.length;
    }

    @Override
    public T get(int index) {

        chekIndex(index);

        return elements[index];
    }


    @Override
    public void add(T element) {

        if (capacity() == size()) {
            this.elements = Arrays.copyOf(this.elements, capacity() * 2);
        }

        this.elements[size()] = element;
        size++;
    }

    @Override
    public boolean contains(T element) {

        /*for (T t : elements) {
            return t == element;
        }*/
        return indexOf(element) != -1;
    }

    @Override
    public int indexOf(T element) {

        /*for (int i = 0; i < size(); i++) {
            if (element == null && get(i) == null) return -1;
            if (get(i).equals(element)) {
                return i;
            }
        }

        return -1;*/

        return IntStream.range(0, size()).takeWhile(i -> element != null || get(i) != null)
                .filter(i -> get(i).equals(element))
                .findFirst().orElse(-1);
    }

    @Override
    public int lastIndexOf(T element) {

        for (int i = size() - 1; i >= 0; i--) {
            if (element == null && get(i) == null) return i;
            if (get(i).equals(element)) return i;
        }

        return -1;
    }

    @Override
    public boolean remove(T element) {

        return removeAt(indexOf(element));
    }

    @Override
    public boolean removeAt(int index) {

        if (index < 0 || index > size()) return false;

        for (int i = index; i < size - 1; i++) {
            elements[i] = elements[i + 1];
        }
        elements[size - 1] = null;
        size--;

        return true;
    }

    @Override
    public void clear() {
        size = 0;
        this.elements = (T[]) new Object[DEFAULT_CAPACITY];
    }

    @Override
    public void swap(int from, int to) {
        T temporary = get(from);
        this.elements[from] = get(to);
        this.elements[to] = temporary;
    }

    @Override
    public void print() {
        StringBuilder builder = new StringBuilder("[");

        for (int i = 0; i < size() - 1; i++) {
            builder.append(i).append(", ");
        }

        System.out.println(builder.append(size() - 1).append("]").toString());
    }

    @Override
    public Iterator<T> iterator() {
        return new MyListIterator();
    }

    private void chekIndex(int index) {
        if (index < 0 || index >= size()) {
            throw new ArrayIndexOutOfBoundsException("Invalid index");
        }
    }

    private class MyListIterator implements Iterator<T> {
        int currentIndex;

        MyListIterator() {
            currentIndex = 0;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < size;
        }

        @Override
        public T next() {
            T result = elements[currentIndex];
            currentIndex++;
            return result;
        }
    }
}
