package com.telerikacademy.oop.tests.MyListImplTests;

import com.telerikacademy.oop.MyList;
import com.telerikacademy.oop.MyListImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class ContainsTests {

    private MyList<Integer> myList;

    @BeforeEach
    public void before() {
        myList = new MyListImpl<>(1);
    }

    @Test
    @DisplayName("contains() should return true when element exists")
    public void contains_should_return_true() {
        myList.add(1);

        Assertions.assertTrue(myList.contains(1));
    }

    @Test
    @DisplayName("contains() should return false when element exists")
    public void contains_should_return_false() {
        Assertions.assertFalse(myList.contains(1));
    }
}
