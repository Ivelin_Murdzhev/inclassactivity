package com.telerikacademy.oop.tests.MyListImplTests;

import com.telerikacademy.oop.MyList;
import com.telerikacademy.oop.MyListImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class RemoveAtTests {

    private MyList<Integer> myList;

    @BeforeEach
    public void before() {
        myList = new MyListImpl<>(1);
    }

    @Test
    @DisplayName("removeAt() removes element and returns true")
    public void removeAt_returns_true_when_successful() {
        myList.add(1);
        myList.add(2);
        myList.add(3);
        myList.add(4);

        Assertions.assertTrue(myList.removeAt(1));
        myList.print();
    }

    @Test
    @DisplayName("removeAt() returns false when element does not exist")
    public void remove_returns_false_when_unsuccessful() {
        myList.add(1);
        myList.add(2);
        myList.add(3);

        Assertions.assertFalse(myList.removeAt(4));
        myList.print();
    }

}
