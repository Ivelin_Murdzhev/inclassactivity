package com.telerikacademy.oop.tests.MyListImplTests;

import com.telerikacademy.oop.MyList;
import com.telerikacademy.oop.MyListImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class IndexOfTests {

    private MyList<Integer> myList;

    @BeforeEach
    public void before() {
        myList = new MyListImpl<>(1);
    }

    @Test
    @DisplayName("indexOf() returns correct index when element exists")
    public void indexOf_should_return_index() {
        myList.add(1);
        myList.add(2);
        myList.add(3);

        Assertions.assertEquals(1, myList.indexOf(2));
    }

    @Test
    @DisplayName("indexOf() returns -1 index when element doesn't exist")
    public void indexOf_should_return_minusOne() {
        myList.add(1);
        myList.add(2);
        myList.add(3);

        Assertions.assertEquals(-1, myList.indexOf(4));
    }
}
