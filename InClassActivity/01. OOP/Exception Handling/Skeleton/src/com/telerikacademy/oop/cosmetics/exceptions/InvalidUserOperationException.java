package com.telerikacademy.oop.cosmetics.exceptions;

public class InvalidUserOperationException extends RuntimeException {

    public InvalidUserOperationException (String message){

        super(message);
    }
}
