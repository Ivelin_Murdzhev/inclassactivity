package com.telerikacademy.oop.cosmetics.core;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.*;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserOperationException;

import java.util.*;

public final class EngineImpl implements Engine {
    private static final String TERMINATION_COMMAND = "Exit";

    private final CommandParser commandParser;
    private final CommandFactory commandFactory;
    private final ProductFactory productFactory;
    private final ProductRepository productRepository;
    private final Reader reader;
    private final Writer writer;

    public EngineImpl() {
        commandParser = new CommandParserImpl();
        commandFactory = new CommandFactoryImpl();
        productFactory = new ProductFactoryImpl();
        productRepository = new ProductRepositoryImpl();
        reader = new ConsoleReader();
        writer = new ConsoleWriter();
    }

    public void start() {
        while (true) {
            String commandLine = reader.readLine();
            if (commandLine.equalsIgnoreCase(TERMINATION_COMMAND)) {
                break;
            }
            processCommand(commandLine);
        }
    }

    private void processCommand(String commandLine) {
        try {
            String commandName = commandParser.parseCommand(commandLine);
            List<String> parameters = commandParser.parseParameters(commandLine);
            Command command = commandFactory.createCommand(commandName, productFactory, productRepository);
            command.execute(parameters);
            writer.writeLine(command.getResult());
        } catch (InvalidUserOperationException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println("Unknown exception has detected");
        }
    }
}
