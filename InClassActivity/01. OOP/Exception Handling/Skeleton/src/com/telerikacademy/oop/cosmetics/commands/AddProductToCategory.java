package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserOperationException;
import com.telerikacademy.oop.cosmetics.models.ValidationHelper;

import java.util.List;

public class AddProductToCategory implements Command {

    private static final int PARAMETERS_COUNT = 2;
    private static final String PRODUCT_ADDED_TO_CATEGORY = "Product %s added to category %s!";
    private static final String CATEGORY_NOT_EXIST_ERROR = "Category %s does not exist.";
    private static final String PRODUCT_NOT_EXIST_ERROR = "Product %s does not exist.";
    private static final String PRODUCT_ERROR = "Product cannot be null";
    private static final String ADD_PRODUCT_PARAMS_ERROR = "AddProductToCategory command expects %d parameters.";

    private final ProductRepository productRepository;
    private String result;

    public AddProductToCategory(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void execute(List<String> parameters) {
        // Validate parameters count
        ValidationHelper.chekValidParameters(parameters, PARAMETERS_COUNT, PRODUCT_ERROR, ADD_PRODUCT_PARAMS_ERROR);
        String categoryNameToAdd = parameters.get(0);
        String productNameToAdd = parameters.get(1);

        result = addProductToCategory(categoryNameToAdd, productNameToAdd);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String addProductToCategory(String categoryName, String productName) {
        // Validate product and category exist
        if (!productRepository.getCategories().containsKey(categoryName)) {
            throw new InvalidUserOperationException(String.format(CATEGORY_NOT_EXIST_ERROR, categoryName));
        }
        if (!productRepository.getProducts().containsKey(productName)) {
            throw new InvalidUserOperationException(String.format(PRODUCT_NOT_EXIST_ERROR, productName));
        }

        Category category = productRepository.getCategories().get(categoryName);
        Product product = productRepository.getProducts().get(productName);

        category.addProduct(product);

        return String.format(PRODUCT_ADDED_TO_CATEGORY, productName, categoryName);
    }
}
