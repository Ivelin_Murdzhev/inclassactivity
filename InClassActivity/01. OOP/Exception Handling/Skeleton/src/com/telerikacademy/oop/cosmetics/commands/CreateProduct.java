package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserOperationException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ValidationHelper;

import java.util.List;

public class CreateProduct implements Command {
    private static final int PARAMETERS_COUNT = 4;
    private static final String PRODUCT_CREATED = "Product with name %s was created!";
    private static final String PRODUCT_EXIST_MESSAGE = "Product %s already exist.";
    private static final String PRODUCT_NULL_ERROR = "CreateProduct cannot be null";
    private static final String CREATE_PRODUCT_PARAMS_ERROR = String.format(
            "CreateProduct command expects %d parameters.",
            PARAMETERS_COUNT);

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateProduct(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    @Override
    public void execute(List<String> parameters) {
        //Validate parameters count
        ValidationHelper.chekValidParameters(parameters, PARAMETERS_COUNT, PRODUCT_NULL_ERROR, CREATE_PRODUCT_PARAMS_ERROR);

        String name = parameters.get(0);
        String brand = parameters.get(1);

        //Validate price format
        if (!parameters.get(2).matches(".*\\d.*")) {
            throw new InvalidUserOperationException("Third parameter should be price (real number).");
        }
        double price = Double.parseDouble(parameters.get(2));
        //Validate gender format
        GenderType gender = null;
        try {
            gender = GenderType.valueOf(parameters.get(3).toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new InvalidUserOperationException("Forth parameter should be one of Men, Women or Unisex.");
        }

        result = createProduct(name, brand, price, gender);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createProduct(String name, String brand, double price, GenderType gender) {
        //Ensure category name is unique
        if (productRepository.getProducts().containsKey(name)) {
            throw new InvalidUserOperationException(String.format(PRODUCT_EXIST_MESSAGE, name));
        }

        Product product = productFactory.createProduct(name, brand, price, gender);
        productRepository.getProducts().put(name, product);

        return String.format(PRODUCT_CREATED, name);
    }
}
