package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserOperationException;
import com.telerikacademy.oop.cosmetics.models.ValidationHelper;

import java.util.List;

public class ShowCategory implements Command {
    private static final int PARAMETERS_COUNT = 1;
    private static final String CATEGORY_NOT_EXIST_ERROR = "Category %s does not exist.";
    private static final String NULL_ERROR = "ShowCategory cannot be null";
    private static final String PARAMS_ERROR = "ShowCategory command expects 1 parameters.";


    private final ProductRepository productRepository;
    private String result;

    public ShowCategory(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void execute(List<String> parameters) {
        // Validate parameters count
        ValidationHelper.chekValidParameters(parameters,PARAMETERS_COUNT, NULL_ERROR, PARAMS_ERROR);

        String categoryName = parameters.get(0);

        result = showCategory(categoryName);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String showCategory(String categoryName) {
        // Validate category exist
        if (!productRepository.getCategories().containsKey(categoryName)) {
            throw new InvalidUserOperationException(String.format(CATEGORY_NOT_EXIST_ERROR, categoryName));
        }

        Category category = productRepository.getCategories().get(categoryName);

        return category.print();
    }
}
