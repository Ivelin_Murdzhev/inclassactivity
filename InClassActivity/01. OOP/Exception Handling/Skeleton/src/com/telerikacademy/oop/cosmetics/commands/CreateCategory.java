package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserOperationException;
import com.telerikacademy.oop.cosmetics.models.ValidationHelper;

import java.util.List;

public class CreateCategory implements Command {
    private static final int PARAMETERS_COUNT = 1;
    private static final String CATEGORY_EXIST_MESSAGE = "Category %s already exist.";
    private static final String CATEGORY_CREATED_MESSAGE = "Category with name %s was created!";
    private static final String CATEGORY_NULL_ERROR = "Category cannot be null";
    private static final String CATEGORY_PARAMS_ERROR = "CreateCategory command expects %d parameters.";

    private final ProductRepository productRepository;
    private final ProductFactory productFactory;
    private String result;

    public CreateCategory(ProductRepository productRepository, ProductFactory productFactory) {
        this.productRepository = productRepository;
        this.productFactory = productFactory;
    }

    @Override
    public void execute(List<String> parameters) {
        // Validate parameters count
        ValidationHelper.chekValidParameters(parameters,PARAMETERS_COUNT, CATEGORY_NULL_ERROR, CATEGORY_PARAMS_ERROR);

        String categoryName = parameters.get(0);

        result = createCategory(categoryName);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String createCategory(String categoryName) {
        // Ensure category name is unique
        if (productRepository.getCategories().containsKey(categoryName)) {
            throw new InvalidUserOperationException(String.format(CATEGORY_EXIST_MESSAGE, categoryName));
        }

        Category category = productFactory.createCategory(categoryName);
        productRepository.getCategories().put(categoryName, category);

        return String.format(CATEGORY_CREATED_MESSAGE, categoryName);
    }
}
