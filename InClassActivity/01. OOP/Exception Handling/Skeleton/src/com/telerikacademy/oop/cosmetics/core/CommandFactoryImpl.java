package com.telerikacademy.oop.cosmetics.core;

import com.telerikacademy.oop.cosmetics.commands.*;
import com.telerikacademy.oop.cosmetics.commands.CommandType;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CommandFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserOperationException;

import java.util.InputMismatchException;

public class CommandFactoryImpl implements CommandFactory {
    @Override
    public Command createCommand(String commandTypeValue, ProductFactory productFactory, ProductRepository productRepository) {
        //Validate command format
        try {
            CommandType commandType = CommandType.valueOf(commandTypeValue.toUpperCase());

            switch (commandType) {
                case CREATECATEGORY:
                    return new CreateCategory(productRepository, productFactory);
                case CREATEPRODUCT:
                    return new CreateProduct(productRepository, productFactory);
                case ADDPRODUCTTOCATEGORY:
                    return new AddProductToCategory(productRepository);
                case SHOWCATEGORY:
                    return new ShowCategory(productRepository);
                default:
                    // Can we improve this code?
                    throw new UnsupportedOperationException(String.format("Command %s is not supported.", commandTypeValue));
            }
        } catch (IllegalArgumentException e) {
        throw new InvalidUserOperationException(String.format("Command %s is not supported.", commandTypeValue));
    }
    }
}
