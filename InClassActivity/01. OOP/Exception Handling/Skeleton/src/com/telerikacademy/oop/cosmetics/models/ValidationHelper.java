package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserOperationException;

import java.util.List;

public final class ValidationHelper {

    public static void validateNameLength(String name, int min, int max) {
        if (name.length() < min || name.length() > max) {

            throw new InvalidUserOperationException("Category name should be between 3 and 10 symbols.");
        }
    }

    public static void validateBrandLength(String name, int min, int max) {
        if (name.length() < min || name.length() > max) {

            throw new InvalidUserOperationException("Product brand should be between 2 and 10 symbols.");
        }
    }

    public static void chekForNegative(double number) {
        if (number < 0) {

            throw new InvalidUserOperationException("Price can't be negative.");
        }
    }

    public static void chekValidParameters(List<String> parameters, int count, String message1, String message2) {
        if (parameters == null) {
            throw new NullPointerException(message1);
        }
        if (parameters.size() < count) {
            throw new InvalidUserOperationException(message2);
        }
    }

}
