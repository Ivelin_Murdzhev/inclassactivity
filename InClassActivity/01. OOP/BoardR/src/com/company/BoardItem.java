package com.company;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public abstract class BoardItem {

    private static final int MIN_TITLE_LENGTH = 5;
    private static final int MAX_TITLE_LENGTH = 30;
    private String title;
    private LocalDate dueDate;
    private Status status;
    protected List<EventLog> events;

    /**
     * Constructor for class BoardItem.
     *
     * @param title String describes what this item is.
     * @param date  LocalDate when it should be done.
     * @param status Enum describes what is the status.
     */
    public BoardItem(String title, LocalDate date, Status status) {
        setTitle(title);
        setDueDate(date);
        this.status = status;
        events = new ArrayList<EventLog>();
        logEvent(viewInfo());
    }

    /**
     * Constructor for class BoardItem.
     * @param title String describes what this item is.
     * @param date LocalDate when it should be done.
     */
    public BoardItem(String title, LocalDate date) {
        this(title, date, Status.Open);
    }

    /**
     * @return current title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set the title and describes what this item is.
     *
     * @param title String.
     * {@link IllegalArgumentException} when title is empty or null.
     * {@link IllegalArgumentException} when title is less than 5 and more than 30 characters.
     */
    void setTitle(String title) {
        if (title == null) {
            throw new IllegalArgumentException("Title should never be empty!");
        }
        if (title.length() > MAX_TITLE_LENGTH || title.length() < MIN_TITLE_LENGTH) {
            throw new IllegalArgumentException("Title should be between 5 and 30 characters!");
        }
        if (this.title != null) {
            logEvent("Title changed from " + this.title + " to " + title);
        }
        this.title = title;
    }

    /**
     * @return current dueDate.
     */
    public LocalDate getDueDate() {
        return dueDate;
    }

    /**
     * Set the Date and must be in the future - expect a task to be finished before we created it.
     * @param date LocalDate when it should be done.
     * {@link IllegalArgumentException} when Date is in the past.
     */
    void setDueDate(LocalDate date) {
        if (date.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("Date should never be in the past!");
        }
        if (this.dueDate != null) {
            logEvent(String.format("DueDate changed from %s to %s",
                    dueDate, date));
        }
        this.dueDate = date;
    }

    /**
     * @return current status.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Set the status.
     * @param status Status.
     */
    void setStatus(Status status) {
        this.status = status;
    }

    /**
     * Revert the Status and return it to a previous state.
     */
    protected abstract void revertStatus();

    /**
     * Advances the Status to a next state.
     */
    protected abstract void advanceStatus();

    /**
     * Display all the history of events.
     */
    String getHistory() {
        StringBuilder builder = new StringBuilder();

        for (EventLog event : events) {
            builder.append(event.viewInfo()).append(System.lineSeparator());
        }
        return builder.toString();
    }

    /**
     * @return information about the current BoardItem in String format.
     */
    public String viewInfo() {
        return String.format("'%s', [%s | %s]",
                title, status, dueDate);
    }

    /**
     * Add the events.
     * @param event String event to be added.
     * @return void.
     */
    protected void logEvent(String event) {
        events.add(new EventLog(event));
    }
}
