package com.company;

import java.time.LocalDate;

public class EventLog {

    private final String description;
    private final LocalDate timestamp = LocalDate.now();

    /**
     * Constructor for class EventLog
     * @param description Describe the given description.
     * {@link IllegalArgumentException} when description is null.
     */
    public EventLog(String description) {
        if (description == null) {
            throw new IllegalArgumentException("Description cannot be null!");
        }
        this.description = description;
    }

    /**
     * Get the description.
     * @return the current description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return information about the current EventLog in String format.
     */
    public String viewInfo() {
        return String.format("[%s] %s", timestamp, description);
    }
}
