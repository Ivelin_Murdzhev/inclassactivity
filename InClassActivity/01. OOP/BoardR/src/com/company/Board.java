package com.company;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private static List<BoardItem> items = new ArrayList<>();
    private static int totalItems;

    /**
     * Constructor for class Board.
     */
    private Board() {
        items = new ArrayList<>();
    }

    /**
     * @return the current number of items.
     */
    static int totalItems() {
        return totalItems;
    }

    /**
     * Adds unique items to collection and count them.
     * .
     *
     * @param item BoardItem object to put in the collection
     * @exception IllegalArgumentException when item is already in this collection
     */
    public static void addItem(BoardItem item){
        if(items.contains(item)){
            throw new IllegalArgumentException("This item is already in the list");
        }else{
            items.add(item);
            totalItems++;
        }
    }
    static void displayHistory(Logger logger){
        for (BoardItem item : items) {
            logger.log(item.getHistory());
        }
    }
}
