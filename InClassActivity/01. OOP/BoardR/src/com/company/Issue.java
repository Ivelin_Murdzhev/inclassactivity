package com.company;

import java.time.LocalDate;

public class Issue extends BoardItem {
    private final Status initialStatus = Status.Open;
    private final Status finalStatus = Status.Verified;
    private String description;

    /**
     * Constructor for class Issue
     *
     * @param title       String extend the functionality of a board item.
     * @param description String describe a problem that needs attention.
     * @param date        LocalDate extend the functionality of a board item.
     */
    public Issue(String title, String description, LocalDate date) {
        super(title, date, Status.Open);
        setDescription(description);
    }

    /**
     * Get Description
     *
     * @return current description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set Description
     *
     * @param description String describe a problem that needs attention.
     */
    private void setDescription(String description) {
        if (description == null) {
            description = "No description";
        }
        if (this.description == null) {
            this.description = "No description";
        }
        this.description = description;
    }

    /**
     * Set Verified to Open and do nothing if already opened and log it.
     */
    @Override
    protected void revertStatus() {
        if (getStatus() != initialStatus) {
            setStatus(initialStatus);
            logEvent(String.format("Issue status set to %s", getStatus()));
        } else {
            logEvent("Issue status already Open");
        }
    }

    /**
     * Set the status as Verified and do nothing if already Verified and log it.
     */
    @Override
    protected void advanceStatus() {
        if (getStatus() != finalStatus) {
            setStatus(finalStatus);
            logEvent(String.format("Issue status set to %s", getStatus()));
        } else {
            logEvent("Issue status already Verified");
        }
    }

    @Override
    public String viewInfo() {
        String baseInfo = super.viewInfo();
        return String.format("Issue: %s, Description: %s",baseInfo, this.getDescription());
    }
}
