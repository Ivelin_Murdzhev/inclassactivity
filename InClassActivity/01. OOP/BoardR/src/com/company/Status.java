package com.company;

public enum Status {
    Open, ToDo, InProgress, Done, Verified
}
