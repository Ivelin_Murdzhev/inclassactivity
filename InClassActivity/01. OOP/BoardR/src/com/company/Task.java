package com.company;

import java.time.LocalDate;

public class Task extends BoardItem {

    private static final int ASSIGNEE_MIN_LENGTH = 5;
    private static final int ASSIGNEE_MAX_LENGTH = 30;
    private String assignee;

    /**
     * Constructor for class Task used to describe work that needs to be done and have its initial status as To Do.
     *
     * @param title String extend the functionality of a board item.
     * @param assignee String
     * @param date LocalDate extend the functionality of a board item.
     */
    public Task(String title, String assignee, LocalDate date) {
        super(title, date, Status.ToDo);
        setAssignee(assignee);
    }

    /**
     * Get the Assignee
     * @return the current assignee.
     */
    public String getAssignee() {
        return assignee;
    }

    /**
     * Set the Assignee
     * @param assignee String
     * {@link IllegalArgumentException} when Assignee is null or empty.
     * {@link IllegalArgumentException} when Assignee is not between 5 and 30 characters.
     */
    void setAssignee(String assignee) {
        if (assignee == null) {
            throw new IllegalArgumentException("Assignee should never be null or empty");
        }
        if (assignee.length() < ASSIGNEE_MIN_LENGTH || assignee.length() > ASSIGNEE_MAX_LENGTH) {
            throw new IllegalArgumentException("Assignee should be between 5 and 30 characters!");
        }
        if(this.assignee != null) {
            super.logEvent(String.format("Assignee changed from %s to %s", this.assignee, assignee));
        }
        this.assignee = assignee;
    }

    /**
     * Revert the Status and return it to a previous state.
     */
    @Override
    protected void revertStatus() {
        if(getStatus() != Status.Open){
            logEvent(String.format("Task status changed from %s to %s",
                    getStatus(), Status.values()[getStatus().ordinal() - 1]));
            setStatus(Status.values()[getStatus().ordinal() - 1]);
        }else {
            logEvent("Can't advance, already at Open");
        }
    }

    /**
     * Advances the Status to a next state.
     */
    @Override
    protected void advanceStatus() {
        if (getStatus() != Status.Verified) {
            logEvent(String.format("Task status changed from %s to %s",
                    getStatus(), Status.values()[getStatus().ordinal() + 1]));
            setStatus(Status.values()[getStatus().ordinal() + 1]);
        } else {
            logEvent("Can't advance, already at Verified");
        }
    }

    @Override
    public String viewInfo() {
        String baseInfo = super.viewInfo();
        return String.format("Task: %s, Assignee: %s",baseInfo,this.getAssignee());
    }
}
