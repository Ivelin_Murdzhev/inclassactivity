package com.telerikacademy.oop.furniture.models;

public final class ValidationHelper {

    private ValidationHelper() {
    }

    public static void chekForNull(Object obj, String message) {
        if (obj == null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void chekIsEmpty(String str, String message) {
        if (str.isEmpty()) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void chekForZero(double number, String message) {
        if (number <= 0) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void chekSymbolsLength(String name, int min, String message) {
        if (name.trim().length() < min) {
            throw new IllegalArgumentException(message);
        }
    }

}
