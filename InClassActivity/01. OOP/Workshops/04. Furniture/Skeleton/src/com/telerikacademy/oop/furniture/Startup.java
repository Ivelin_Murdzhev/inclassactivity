package com.telerikacademy.oop.furniture;

import com.telerikacademy.oop.furniture.core.FurnitureEngineImpl;

public class Startup {
    
    public static void main(String[] args) {
        FurnitureEngineImpl engine = new FurnitureEngineImpl();
        engine.start();
    }
    
}
