package com.telerikacademy.oop.furniture.commands;

import com.telerikacademy.oop.furniture.commands.contracts.Command;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureFactory;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureRepository;
import com.telerikacademy.oop.furniture.models.contracts.Table;

import java.util.List;

public class CreateTable implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 6;

    private final FurnitureRepository furnitureRepository;
    private final FurnitureFactory furnitureFactory;

    public CreateTable(FurnitureRepository furnitureRepository, FurnitureFactory furnitureFactory) {
        this.furnitureRepository = furnitureRepository;
        this.furnitureFactory = furnitureFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(CommandConstants.INVALID_NUMBER_OF_ARGUMENTS);
        }

        String tableModel = parameters.get(0);
        String tableMaterial = parameters.get(1);
        double tablePrice = Double.parseDouble(parameters.get(2));
        double tableHeight = Double.parseDouble(parameters.get(3));
        double tableLength = Double.parseDouble(parameters.get(4));
        double tableWidth = Double.parseDouble(parameters.get(5));
        return createTable(tableModel, tableMaterial, tablePrice, tableHeight, tableLength, tableWidth);
    }

    private String createTable(String model, String materialType, double price, double height, double length, double width) {
        if (furnitureRepository.getFurnitures().containsKey(model)) {
            return String.format(CommandConstants.FURNITURE_EXISTS_ERROR_MESSAGE, model);
        }

        Table table = furnitureFactory.createTable(model, materialType, price, height, length, width);
        furnitureRepository.addFurniture(model, table);

        return String.format(CommandConstants.TABLE_CREATED_SUCCESS_MESSAGE, model);
    }
}
