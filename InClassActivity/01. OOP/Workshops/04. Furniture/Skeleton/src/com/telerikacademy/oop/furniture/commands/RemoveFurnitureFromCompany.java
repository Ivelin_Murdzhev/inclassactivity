package com.telerikacademy.oop.furniture.commands;

import com.telerikacademy.oop.furniture.commands.contracts.Command;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureFactory;
import com.telerikacademy.oop.furniture.core.contracts.FurnitureRepository;
import com.telerikacademy.oop.furniture.models.contracts.Company;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;

import java.util.List;

public class RemoveFurnitureFromCompany implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final FurnitureRepository furnitureRepository;
    private final FurnitureFactory furnitureFactory;

    public RemoveFurnitureFromCompany(FurnitureRepository furnitureRepository, FurnitureFactory furnitureFactory) {
        this.furnitureRepository = furnitureRepository;
        this.furnitureFactory = furnitureFactory;
    }

    @Override
    public String execute(List<String> parameters) {

        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(CommandConstants.INVALID_NUMBER_OF_ARGUMENTS);
        }
        String companyToRemoveFrom = parameters.get(0);
        String furnitureToBeRemoved = parameters.get(1);
        return removeFurnitureFromCompany(companyToRemoveFrom, furnitureToBeRemoved);
    }

    private String removeFurnitureFromCompany(String companyName, String furnitureName) {
        if (!furnitureRepository.getCompanies().containsKey(companyName)) {
            return String.format(CommandConstants.COMPANY_NOT_FOUND_ERROR_MESSAGE, companyName);
        }

        if (!furnitureRepository.getFurnitures().containsKey(furnitureName)) {
            return String.format(CommandConstants.FURNITURE_NOT_FOUND_ERROR_MESSAGE, furnitureName);
        }

        Company company = furnitureRepository.getCompanies().get(companyName);
        Furniture furniture = furnitureRepository.getFurnitures().get(furnitureName);
        company.remove(furniture);

        return String.format(CommandConstants.FURNITURE_REMOVED_SUCCESS_MESSAGE, furnitureName, companyName);
    }

}
