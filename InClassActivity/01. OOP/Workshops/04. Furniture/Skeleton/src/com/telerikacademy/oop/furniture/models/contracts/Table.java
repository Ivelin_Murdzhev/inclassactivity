package com.telerikacademy.oop.furniture.models.contracts;

public interface Table extends Furniture {
    
    double getLength();
    
    double getWidth();
    
    double getArea();
    
}
