package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.ConvertibleChair;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class ConvertibleChairImpl extends ChairImpl implements ConvertibleChair {

    private static final double CONVERTED_HEIGHT = 0.10;
    private boolean isConverted;
    private final double initialHeight;

    public ConvertibleChairImpl(String model, MaterialType material, double price, double height, int numberOfLegs) {
        super(model, material, price, height, numberOfLegs);
        this.initialHeight = height;
        this.isConverted = false;
    }

    @Override
    public boolean getConverted() {

        return isConverted;
    }

    @Override
    public void convert() {
        isConverted = !isConverted;
        if (isConverted) {
            setHeight(CONVERTED_HEIGHT);
        } else {
            setHeight(initialHeight);
        }
    }

    @Override
    protected String additionalInfo() {
        return String.format("%s, State: %s",
                super.additionalInfo(),
                getConverted() ? "Converted" : "Normal");
    }

}
