package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public abstract class FurnitureBase implements Furniture {

    private static final int MODEL_MIN_SYMBOLS_LENGTH = 3;
    private static final double PRICE_MIN_LIMIT = 0.00;
    private static final double HEIGHT_MIN_LIMIT = 0.00;
    private static final String MODEL_EMPTY_ERROR = "Model cannot be empty";
    private static final String MODEL_NULL_ERROR = "Model cannot be null";
    private static final String MODEL_MIN_SYMBOLS_ERROR = "Model cannot be less than 3 symbols.";
    private static final String PRICE_ERROR = "Price cannot be less or equal to 0.00.";
    private static final String HEIGHT_ERROR = "Height cannot be less or equal to 0.00";


    private String model;
    private MaterialType material;
    private double price;
    private double height;

    public FurnitureBase(String model, MaterialType material, double price, double height) {
        setModel(model);
        setPrice(price);
        setHeight(height);
        this.material = material;
    }

    @Override
    public String getModel() {
        return this.model;
    }

    @Override
    public MaterialType getMaterialType() {
        return material;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public double getHeight() {
        return this.height;
    }

    @Override
    public String toString() {
        return String.format("Type: %s, Model: %s, Material: %s, Price: %.2f, Height: %.2f%s",
                this.getClass().getSimpleName().replace("Impl", ""),
                getModel(), getMaterialType(), getPrice(), getHeight(), additionalInfo());
    }

    protected abstract String additionalInfo();

    protected void setHeight(double height) {
        if (height <= HEIGHT_MIN_LIMIT) {
            throw new IllegalArgumentException(HEIGHT_ERROR);
        }

        this.height = height;
    }

    private void setModel(String model) {
        ValidationHelper.chekForNull(model, MODEL_NULL_ERROR);
        ValidationHelper.chekIsEmpty(model, MODEL_EMPTY_ERROR);
        ValidationHelper.chekSymbolsLength(model, MODEL_MIN_SYMBOLS_LENGTH, MODEL_MIN_SYMBOLS_ERROR);

        this.model = model.trim();
    }

    private void setPrice(double price) {
        if (price <= PRICE_MIN_LIMIT) {
            throw new IllegalArgumentException(PRICE_ERROR);
        }

        this.price = price;
    }
}
