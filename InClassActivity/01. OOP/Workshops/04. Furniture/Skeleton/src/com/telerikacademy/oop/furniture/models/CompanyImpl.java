package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Company;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CompanyImpl implements Company {

    private static final int COMPANY_MIN_SYMBOLS_LENGTH = 5;
    private static final int EXACT_REGISTRATION_NUMBER_LENGTH = 10;
    private static final String COMPANY_EMPTY_ERROR = "Company cannot be empty";
    private static final String COMPANY_NULL_ERROR = "Company cannot be null";
    private static final String COMPANY_MIN_SYMBOLS_ERROR = "Company cannot be less than 5 symbols.";
    private static final String MODEL_ERROR = "Model cannot be null";
    private static final String REG_NUMBER_ERROR = "Registration number is not valid";

    private String name;
    private String registrationNumber;
    private List<Furniture> furnitures;

    public CompanyImpl(String name, String registrationNumber) {
        setName(name);
        setRegistrationNumber(registrationNumber);
        furnitures = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public List<Furniture> getFurnitures() {

        return new ArrayList<>(furnitures);
    }

    public void add(Furniture furniture) {
        ValidationHelper.chekForNull(furniture, "Furniture cannot be null");
        furnitures.add(furniture);
    }

    public String catalog() {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(String.format("%s - %s - %s %s",
                name,
                registrationNumber,
                furnitures.isEmpty() ? "no" : String.format("%d", furnitures.size()),
                furnitures.size() == 1 ? "furniture" : "furnitures"));

        strBuilder.append(System.lineSeparator());

        furnitures.sort(Comparator.comparing(Furniture::getPrice)
                .thenComparing(Furniture::getModel));

        for (Furniture furniture : furnitures) {
            strBuilder.append(furniture.toString())
                    .append(System.lineSeparator());
        }

        return strBuilder.toString().trim();
    }

    public Furniture find(String model) {
        ValidationHelper.chekForNull(model, MODEL_ERROR);
        for (Furniture furniture : furnitures) {
            if (furniture.getModel().equalsIgnoreCase(model))
                return furniture;
        }
        return null;
    }

    public void remove(Furniture furniture) {
        furnitures.removeIf(furniture1 -> furniture == furniture1);
    }


    private void setRegistrationNumber(String registrationNumber) {
        ValidationHelper.chekForNull(registrationNumber, "Registration number cannot be null");
        if (registrationNumber.length() != EXACT_REGISTRATION_NUMBER_LENGTH) {
            throw new IllegalArgumentException(REG_NUMBER_ERROR);
        }
        if (!registrationNumber.chars().allMatch(Character::isDigit)) {
            throw new IllegalArgumentException(REG_NUMBER_ERROR);
        }

        this.registrationNumber = registrationNumber;
    }

    private void setName(String name) {
        ValidationHelper.chekForNull(name, COMPANY_NULL_ERROR);
        ValidationHelper.chekIsEmpty(name, COMPANY_EMPTY_ERROR);
        ValidationHelper.chekSymbolsLength(name, COMPANY_MIN_SYMBOLS_LENGTH, COMPANY_MIN_SYMBOLS_ERROR);

        this.name = name.trim();
    }

    /*@Override
    public String toString() {
        return String.format(
                "%s - %s - %s %s",
                name,
                registrationNumber,
                furnitures.isEmpty() ? "no" : String.format("%d", furnitures.size()),
                furnitures.size() == 1 ? "furniture" : "furnitures"
        );

    }*/

}
