package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Table;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class TableImpl extends FurnitureBase implements Table {

    private static final String LENGTH_ERROR = "Length cannot be less or equal to 0.00.";
    private static final String WIDTH_ERROR = "Width cannot be less or equal to 0.00.";

    private double length;
    private double width;

    public TableImpl(String model, MaterialType material, double price, double height, double length, double width) {
        super(model, material, price, height);
        setLength(length);
        setWidth(width);
    }

    @Override
    public double getLength() {
        return this.length;
    }

    @Override
    public double getWidth() {
        return this.width;
    }

    @Override
    public double getArea() {
        return getLength() * getWidth();
    }

    @Override
    protected String additionalInfo() {
        return String.format(", Length: %.2f, Width: %.2f, Area: %.4f",
                getLength(), getWidth(), getArea());
    }

    private void setLength(double length) {
        ValidationHelper.chekForZero(length, LENGTH_ERROR);

        this.length = length;
    }

    private void setWidth(double width) {
        ValidationHelper.chekForZero(width, WIDTH_ERROR);

        this.width = width;
    }
}
