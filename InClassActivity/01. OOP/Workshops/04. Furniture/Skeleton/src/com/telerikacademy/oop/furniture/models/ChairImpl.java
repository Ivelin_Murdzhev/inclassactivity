package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.Chair;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class ChairImpl extends FurnitureBase implements Chair {

    private static final String LEGS_ERROR = " A chair must have a legs.";

    private int numberOfLegs;


    public ChairImpl(String model, MaterialType material, double price, double height, int numberOfLegs) {
        super(model, material, price, height);
        setNumberOfLegs(numberOfLegs);
    }

    @Override
    public int getNumberOfLegs() {
        return this.numberOfLegs;
    }

    @Override
    protected String additionalInfo() {
        return String.format(", Legs: %d", getNumberOfLegs());
    }

    private void setNumberOfLegs(int numberOfLegs) {
        if (numberOfLegs > 0) {
            this.numberOfLegs = numberOfLegs;
        } else
            throw new IllegalArgumentException(LEGS_ERROR);
    }
}
