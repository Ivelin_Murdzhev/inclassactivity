package com.telerikacademy.oop.furniture.core.contracts;

public interface Writer {
    
    void write(String message);
    
    void writeLine(String message);
    
}
