package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.contracts.AdjustableChair;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;

public class AdjustableChairImpl extends ChairImpl implements AdjustableChair {

    public AdjustableChairImpl(String model, MaterialType material, double price, double height, int numberOfLegs) {
        super(model, material, price, height, numberOfLegs);
    }

    @Override
    public void setHeight(double height) {
        super.setHeight(height);
    }

}
