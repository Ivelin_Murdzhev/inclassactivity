package com.telerikacademy.oop.furniture.models;

import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TableImpl_Tests {
    
    @Test
    public void constructor_should_throwError_when_lengthNotPositive() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TableImpl("Test", MaterialType.LEATHER, 2, 2, -1, 4));
    }
    
    @Test
    public void constructor_should_throwError_when_lengthIsZero() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TableImpl("Test", MaterialType.LEATHER, 2, 2, 0, 4));
    }
    
    @Test
    public void constructor_should_throwError_when_widthNotPositive() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TableImpl("Test", MaterialType.LEATHER, 2, 2, 4, -4));
    }
    
    @Test
    public void constructor_should_throwError_when_widthIsZero() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TableImpl("Test", MaterialType.LEATHER, 2, 2, 4, 0));
    }
    
}
