package com.telerikacademy.oop.furniture.models;


import com.telerikacademy.oop.furniture.models.contracts.Company;
import com.telerikacademy.oop.furniture.models.contracts.Furniture;
import com.telerikacademy.oop.furniture.models.enums.MaterialType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class CompanyImpl_Tests {
    
    private Furniture testFurniture;
    
    @BeforeEach
    public void before() {
        testFurniture = new ChairImpl("model", MaterialType.LEATHER, 3, 10, 4);
    }
    
    @Test
    public void constructor_should_throwError_when_NameIsNull() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CompanyImpl(null, "1231231232"));
    }
    
    @Test
    public void constructor_should_throwError_when_registrationNumberLengthIsMoreThanRequired() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CompanyImpl("TestName", "12322123121232"));
    }
    
    @Test
    public void constructor_should_throwError_when_registrationNumberLengthIsLessThanRequired() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CompanyImpl("TestName", "111111111"));
    }
    
    @Test
    public void constructor_should_throwError_when_registrationNumberContainsInvalidChars() {
        // Arrange, Acr, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CompanyImpl("TestName", "v51a12545f"));
    }
    
    @Test
    public void constructor_should_throwError_when_nameLengthIsLessThanMin() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CompanyImpl(new String(new char[4]), "1231231232"));
    }
    
    @Test
    public void constructor_should_returnCompany_when_valuesAreValid() {
        // Arrange, Act
        Company company = new CompanyImpl("Telerik", "1556565654");
        
        // Assert
        Assertions.assertEquals(company.getName(), "Telerik");
    }
    
    @Test
    public void getFurnitures_should_returnShallowCopy() {
        // Arrange
        Company company = new CompanyImpl("Telerik", "1556565654");
        
        // Act
        List<Furniture> supposedShallowCopy = company.getFurnitures();
        company.add(testFurniture);
        
        // Assert
        Assertions.assertEquals(0, supposedShallowCopy.size());
    }
    
    @Test
    public void find_should_throwException_when_passedNull() {
        // Arrange
        Company company = new CompanyImpl("Telerik", "1556565654");
        
        // Act & Assert
        
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> company.find(null));
        
    }
    
    @Test
    public void find_should_returnFurniture_when_passedValidModel() {
        // Arrange
        Company company = new CompanyImpl("Telerik", "1556565654");
        company.add(testFurniture);
        // Act
        Furniture found = company.find("model");
        
        // Assert
        Assertions.assertEquals(testFurniture.getModel(), found.getModel());
    }
    
    @Test
    public void find_should_returnNull_when_passedNonPresentModel() {
        // Arrange
        Company company = new CompanyImpl("Telerik", "1556565654");
        company.add(testFurniture);
        
        // Act
        Furniture found = company.find("model123");
        
        // Assert
        Assertions.assertNull(found);
    }
    
    @Test
    public void remove_should_removeFurniture_when_passedPresentFurniture() {
        // Arrange
        Company company = new CompanyImpl("Telerik", "1556565654");
        company.add(testFurniture);
        
        // Act
        company.remove(testFurniture);
        
        // Assert
        Assertions.assertEquals(0, company.getFurnitures().size());
    }
    
    
}
