package com.telerikacademy.oop.cosmetics.models.cart;

import com.telerikacademy.oop.cosmetics.models.HelpClassValidation;
import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    
    private List<Product> productList;
    
    public ShoppingCart() {
        productList = new ArrayList<>();
    }
    
    public List<Product> getProductList() {

        return new ArrayList<>(productList);
    }
    
    public void addProduct(Product product) {
        HelpClassValidation.validateNull(product);
        productList.add(product);
    }
    
    public void removeProduct(Product product) {
        HelpClassValidation.validateNull(product);
        productList.remove(product);
    }
    
    public boolean containsProduct(Product product) {
        HelpClassValidation.validateNull(product);
        return productList.contains(product);
    }
    
    public double totalPrice() {
        double result = 0;
        for (Product product : productList) {
            result += product.getPrice();
        }
        return result;
    }
    
}
