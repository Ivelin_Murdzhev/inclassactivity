package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {

    private static final int MIN_CATEGORY_NAME_LENGTH = 2;
    private static final int MAX_CATEGORY_NAME_LENGTH = 15;
    private String name;
    private List<Product> products;

    public Category(String name) {
        setName(name);
        this.name = name;
        products = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        HelpClassValidation.validateNull(name);
        HelpClassValidation.chekLength(name, MIN_CATEGORY_NAME_LENGTH, MAX_CATEGORY_NAME_LENGTH);
        this.name = name;
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException("Product cannot be null");
        }
        products.add(product);
    }

    public void removeProduct(Product product) {
        if (!products.remove(product)) {
            throw new IllegalArgumentException("Product is not found.");
        }
    }

    public String print() {
        StringBuilder myBuilder = new StringBuilder(String.format("#Category: %s%n", this.name));
        if (products.isEmpty()) {
            myBuilder.append("No product in this category");
        }
        for (Product product : products) {
            myBuilder.append(product.print());
        }
        return myBuilder.toString();
    }

}
