package com.telerikacademy.oop.cosmetics.models;

public class HelpClassValidation {

    public static void validateNull(Object object) {
        if (object == null) {
            throw new IllegalArgumentException("The input cannot be null");
        }
    }

    public static void chekLength(String object, int minLength, int maxLength) {
        if (object.trim().length() < minLength || object.trim().length() > maxLength) {
            throw new IllegalArgumentException(String.format
                    ("Yhe input length must be between %d and %d", minLength, maxLength));
        }
    }
}
