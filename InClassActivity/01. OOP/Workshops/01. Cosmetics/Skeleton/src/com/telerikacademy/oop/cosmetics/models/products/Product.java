package com.telerikacademy.oop.cosmetics.models.products;

import com.telerikacademy.oop.cosmetics.models.HelpClassValidation;
import com.telerikacademy.oop.cosmetics.models.common.GenderType;

public class Product {

    private static final int MIN_PRODUCT_LENGTH = 3;
    private static final int MAX_PRODUCT_LENGTH = 10;
    private static final int MIN_BRAND_NAME_LENGTH = 2;
    private static final int MAX_BRAND_NAME_LENGTH = 10;

    private double price;
    private String name;
    private String brand;
    private GenderType gender;

    public Product(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    public GenderType getGender() {
        return gender;
    }

    private void setGender(GenderType gender) {
        HelpClassValidation.validateNull(gender);
        this.gender = gender;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative!");
        }
        this.price = price;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        HelpClassValidation.validateNull(name);
        HelpClassValidation.chekLength(name, MIN_PRODUCT_LENGTH, MAX_PRODUCT_LENGTH);
        this.name = name.trim();
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        HelpClassValidation.validateNull(brand);
        HelpClassValidation.chekLength(brand, MIN_BRAND_NAME_LENGTH, MAX_BRAND_NAME_LENGTH);
        this.brand = brand.trim();
    }

    public String print() {
        return String.format("# %s %s%n #Price: %.2f%n  #Gender: [%s]%n ===",
                name, brand, price, gender);
    }

}
