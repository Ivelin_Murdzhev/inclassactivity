package com.telerikacademy.cosmetics.models.products;

public class ValidateHelper {

    protected static final int BRAND_MIN_LENGTH = 2;
    protected static final int BRAND_MAX_LENGTH = 10;
    protected static final int NAME_MIN_LENGTH = 3;
    protected static final int NAME_MAX_LENGTH = 10;

    protected static void validateNull(String str) {
        if (str == null)
            throw new IllegalArgumentException("Product cannot be null!");
    }

    protected static void validateLength(String str, int min, int max) {
        if (str.length() < min || str.length() > max)
            throw new IllegalArgumentException(
                    String.format("The name should be between %d and %d characters.", min, max));
    }

    protected static void validateNegative(double number) {
        if (number < 0) {
            throw new IllegalArgumentException("Price cannot be negative!");
        }
    }
}
