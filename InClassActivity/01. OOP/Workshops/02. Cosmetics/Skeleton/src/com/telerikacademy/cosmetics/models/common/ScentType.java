package com.telerikacademy.cosmetics.models.common;

public enum ScentType {
    LAVENDER,
    VANILLA,
    ROSE;

    @Override
    public String toString() {
        switch (this) {
            case LAVENDER:
                return "Lavender";
            case VANILLA:
                return "Vanilla";
            case ROSE:
                return "Rose";
            default:
                throw new IllegalArgumentException();
        }
    }
}
