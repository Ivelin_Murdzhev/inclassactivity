package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Product;

public class CreamImpl extends ProductImpl implements Product {

    private ScentType scentType;

    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scentType) {
        super(name, brand, price, gender);
        setScentType(scentType);
    }

    public ScentType getScentType() {
        return scentType;
    }

    private void setScentType(ScentType scentType) {
        this.scentType = scentType;
    }

    @Override
    public String print() {
        return String.format("#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n" +
                        " #Scent: %s%n"
                , getName(), getBrand(), getPrice(), getGender(), getScentType());
    }
}
