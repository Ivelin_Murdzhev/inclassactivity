package com.telerikacademy.cosmetics.core.contracts;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.*;
import com.telerikacademy.cosmetics.models.products.CreamImpl;


import java.util.List;

public interface CosmeticsFactory {
    Category createCategory(String name);

    Shampoo createShampoo(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage);

    Toothpaste createToothpaste(String name, String brand, double price, GenderType gender, List<String> ingredients);

    CreamImpl createCream(String name, String brand, double price, GenderType gender, ScentType scent);

    ShoppingCart createShoppingCart();
}
