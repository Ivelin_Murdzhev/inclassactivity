package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;

public abstract class ProductImpl implements Product {

    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    public ProductImpl(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    @Override
    public String getName() {
        return name;
    }

    private void setName(String name) {
        ValidateHelper.validateNull(name);
        ValidateHelper.validateLength(name.trim(), ValidateHelper.NAME_MIN_LENGTH, ValidateHelper.NAME_MAX_LENGTH);
        this.name = name.trim();
    }

    @Override
    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        ValidateHelper.validateNull(brand);
        ValidateHelper.validateLength(brand.trim(), ValidateHelper.BRAND_MIN_LENGTH, ValidateHelper.BRAND_MAX_LENGTH);
        this.brand = brand.trim();
    }

    @Override
    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        ValidateHelper.validateNegative(price);
        this.price = price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }
}
