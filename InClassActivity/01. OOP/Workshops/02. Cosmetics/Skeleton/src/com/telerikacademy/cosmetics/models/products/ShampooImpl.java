package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;

public class ShampooImpl extends ProductImpl implements Shampoo {

    private int milliliters;
    private UsageType usage;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {
        super(name, brand, price, gender);
        setMilliliters(milliliters);
        setUsage(usage);
    }

    public int getMilliliters() {
        return milliliters;
    }

    private void setMilliliters(int milliliters) {
        ValidateHelper.validateNegative(milliliters);
        this.milliliters = milliliters;
    }

    public UsageType getUsage() {
        return usage;
    }

    private void setUsage(UsageType usage) {
        this.usage = usage;
    }

    @Override
    public String print() {
        return String.format("#%s %s%n" +
                        " #Price: $%.2f%n" +
                        " #Gender: %s%n" +
                        " #Milliliters: %d%n" +
                        " #Usage: %s%n",
                getName(), getBrand(), getPrice(), getGender(), getMilliliters(), getUsage());
    }
}
