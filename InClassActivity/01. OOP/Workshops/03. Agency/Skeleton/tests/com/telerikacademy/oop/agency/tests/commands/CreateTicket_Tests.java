package com.telerikacademy.oop.agency.tests.commands;

import com.telerikacademy.oop.agency.commands.contracts.Command;
import com.telerikacademy.oop.agency.commands.creation.CreateTicketCommand;
import com.telerikacademy.oop.agency.core.AgencyRepositoryImpl;
import com.telerikacademy.oop.agency.core.contracts.AgencyFactory;
import com.telerikacademy.oop.agency.core.contracts.AgencyRepository;
import com.telerikacademy.oop.agency.core.factories.AgencyFactoryImpl;
import com.telerikacademy.oop.agency.models.JourneyImpl;
import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.vehicles.BusImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateTicket_Tests {
    
    private AgencyRepository agencyRepository;
    private AgencyFactory agencyFactory;
    private Command testCommand;
    
    @BeforeEach
    public void before() {
        this.agencyFactory = new AgencyFactoryImpl();
        this.agencyRepository = new AgencyRepositoryImpl();
        this.testCommand = new CreateTicketCommand(agencyFactory, agencyRepository);
    }
    
    @Test
    public void execute_should_throwException_when_passedFewerArgumentsThanExpected() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("5");
        
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }
    
    @Test
    public void execute_should_throwException_when_passedMoreArgumentsThanExpected() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("5");
        arguments.add("2");
        arguments.add("5");
        
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }
    
    @Test
    public void execute_should_throwException_when_passedInvalidIndex() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("h");
        arguments.add("2");
        agencyRepository.addJourney(null);
        
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }
    
    @Test
    public void execute_should_throwException_when_passedInvalidAdministrativeCosts() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("0");
        arguments.add("h");
        agencyRepository.addJourney(null);
        
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> testCommand.execute(arguments));
    }
    
    @Test
    public void execute_should_createTicket_when_passedValidInput() {
        // Arrange
        List<String> arguments = new ArrayList<>();
        arguments.add("0");
        arguments.add("2");
        agencyRepository.addJourney(
                //look
                (Journey) new JourneyImpl(
                        "start",
                        "destination",
                        5,
                        new BusImpl(20, 1)));
        
        // Act
        testCommand.execute(arguments);
        
        // Assert
        Assertions.assertEquals("TicketImpl", agencyRepository.getTickets().get(0).getClass().getSimpleName());
    }
    
}
