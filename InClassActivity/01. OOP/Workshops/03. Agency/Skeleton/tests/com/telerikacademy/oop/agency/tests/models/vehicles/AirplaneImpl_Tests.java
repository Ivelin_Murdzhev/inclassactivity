package com.telerikacademy.oop.agency.tests.models.vehicles;

import com.telerikacademy.oop.agency.models.vehicles.AirplaneImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AirplaneImpl_Tests {
    
    @Test
    public void constructor_should_throw_when_passengerCapacityLessThanMinValue() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new AirplaneImpl(0, 2, true));
    }
    
    @Test
    public void constructor_should_throw_when_passengerCapacityMoreThanMaxValue() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new AirplaneImpl(801, 2, true));
    }
    
    @Test
    public void constructor_should_throw_when_pricePerKmLessThanMinimum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new AirplaneImpl(100, 0, true));
    }
    
    @Test
    public void constructor_should_throw_when_pricePerKmMoreThanMaximum() {
        // Act
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new AirplaneImpl(100, 3, true));
    }
    
}
