package com.telerikacademy.oop.agency.models;

public final class ValidationHelper {

    public static void validatePassengerCapacity(int capacity, int min, int max, String message) {
        if (capacity < min || capacity > max) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validatePricePerKilometer(double price){
        if (price < 0.10 || price > 2.50) {
            throw new IllegalArgumentException("A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!");
        }
    }

    public static void validateVehiclesCapacity(int capacity){
        if(capacity < 1 || capacity > 800){
            throw new IllegalArgumentException("A vehicle with less than 1 passengers or more than 800 passengers cannot exist!");
        }
    }

    public static void validateNumberInQuantity(int carts, int min, int max, String message){
        if (carts < min || carts > max) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateLocationAndDestination(String str, int min, int max, String message){
        if(str == null){
            throw new IllegalArgumentException("Input cannot be null");
        }
        if (str.length() < min || str.length() > max) {
            throw new IllegalArgumentException(message);
        }
    }

}
