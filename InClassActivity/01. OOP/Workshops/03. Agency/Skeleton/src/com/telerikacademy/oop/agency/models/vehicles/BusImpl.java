package com.telerikacademy.oop.agency.models.vehicles;


import com.telerikacademy.oop.agency.models.ValidationHelper;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    private static final int MIN_CAPACITY = 10;
    private static final int MAX_CAPACITY = 50;
    private static final String CAPACITY_MESSAGE = String.format(
            "A bus cannot have less than %d passengers or more than %d passengers.",
            MIN_CAPACITY, MAX_CAPACITY);

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        ValidationHelper.validatePassengerCapacity(passengerCapacity,MIN_CAPACITY,MAX_CAPACITY,CAPACITY_MESSAGE);
        ValidationHelper.validatePricePerKilometer(pricePerKilometer);
    }

    @Override
    public String toString() {
        return String.format("Bus ----%n" +
                        "Passenger capacity: %d%n" +
                        "Price per kilometer: %.2f%n" +
                        "Vehicle type: %s%n" ,
                getPassengerCapacity(),getPricePerKilometer(),getType());
    }

}
