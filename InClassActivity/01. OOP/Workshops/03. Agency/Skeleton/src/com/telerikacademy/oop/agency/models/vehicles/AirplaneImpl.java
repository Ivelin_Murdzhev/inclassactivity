package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {

    private boolean hasFreeFoods;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer, VehicleType.AIR);
        this.hasFreeFoods = hasFreeFood;
    }

    @Override
    public boolean hasFreeFood() {

        return this.hasFreeFoods;
    }

    @Override
    public String toString() {
        return String.format("Airplane ----%n" +
                        "Passenger capacity: %d%n" +
                        "Price per kilometer: %.2f%n" +
                        "Vehicle type: %s%n" +
                        "Has free food: %b%n",
                getPassengerCapacity(), getPricePerKilometer(), getType(), hasFreeFood());
    }
}
