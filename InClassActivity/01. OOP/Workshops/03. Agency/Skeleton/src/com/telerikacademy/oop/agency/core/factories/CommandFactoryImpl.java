package com.telerikacademy.oop.agency.core.factories;

import com.telerikacademy.oop.agency.commands.contracts.Command;
import com.telerikacademy.oop.agency.commands.creation.*;
import com.telerikacademy.oop.agency.commands.enums.CommandType;
import com.telerikacademy.oop.agency.commands.listing.ListJourneysCommand;
import com.telerikacademy.oop.agency.commands.listing.ListTicketsCommand;
import com.telerikacademy.oop.agency.commands.listing.ListVehiclesCommand;
import com.telerikacademy.oop.agency.core.contracts.AgencyFactory;
import com.telerikacademy.oop.agency.core.contracts.AgencyRepository;
import com.telerikacademy.oop.agency.core.contracts.CommandFactory;

public class CommandFactoryImpl implements CommandFactory {

    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    public Command createCommand(String commandName, AgencyFactory agencyFactory, AgencyRepository agencyRepository) {
        CommandType commandType = CommandType.valueOf(commandName.toUpperCase());
        switch (commandType) {

            case CREATEBUS:
                return new CreateBusCommand(agencyFactory, agencyRepository);

            case CREATEJOURNEY:
                return new CreateJourneyCommand(agencyFactory, agencyRepository);

            case CREATETRAIN:
                return new CreateTrainCommand(agencyFactory, agencyRepository);

            case CREATETICKET:
                return new CreateTicketCommand(agencyFactory, agencyRepository);

            case CREATEAIRPLANE:
                return new CreateAirplaneCommand(agencyFactory, agencyRepository);

            case LISTJOURNEYS:
                return new ListJourneysCommand(agencyRepository);

            case LISTTICKETS:
                return new ListTicketsCommand(agencyRepository);

            case LISTVEHICLES:
                return new ListVehiclesCommand(agencyRepository);


        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandName));
    }

}