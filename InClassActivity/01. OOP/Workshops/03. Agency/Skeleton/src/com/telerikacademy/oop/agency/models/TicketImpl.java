package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {

    private Journey journey;
    private double administrativeCosts;

    public TicketImpl(Journey journey, double administrativeCosts) {
        this.journey = journey;
        setAdministrativeCosts(administrativeCosts);
    }

    private void setAdministrativeCosts(double administrativeCosts) {
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public double getAdministrativeCosts() {
        return this.administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    @Override
    public double calculatePrice() {

        return getAdministrativeCosts() * journey.calculateTravelCosts();
    }

    @Override
    public String toString() {
        return String.format("Ticket ----%n" +
                        "Destination: %s%n" +
                        "Price: %.2f%n",
                journey.getDestination(), calculatePrice());
    }

}
