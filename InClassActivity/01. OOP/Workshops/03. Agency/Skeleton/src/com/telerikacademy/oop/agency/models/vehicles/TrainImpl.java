package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.ValidationHelper;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    private static final int MIN_CAPACITY = 30;
    private static final int MAX_CAPACITY = 150;
    private static final int MIN_CARTS = 1;
    private static final int MAX_CARTS = 15;
    private static final String CAPACITY_MESSAGE = String.format(
            "A train cannot have less than %d passengers or more than %d passengers.",
            MIN_CAPACITY, MAX_CAPACITY);
    private static final String CARTS_MESSAGE = String.format(
            "A train cannot have less than %d cart or more than %d carts.",
            MIN_CARTS, MAX_CARTS);

    private int carts;

    public TrainImpl(int passengerCapacity, int carts, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        ValidationHelper.validatePassengerCapacity(passengerCapacity, MIN_CAPACITY, MAX_CAPACITY, CAPACITY_MESSAGE);
        setCarts(carts);
    }

    private void setCarts(int carts) {
        ValidationHelper.validateNumberInQuantity(carts, MIN_CARTS, MAX_CARTS, CARTS_MESSAGE);

        this.carts = carts;
    }

    @Override
    public int getCarts() {
        return this.carts;
    }


    @Override
    public String toString() {
        return String.format("Train ----%n" +
                        "Passenger capacity: %d%n" +
                        "Price per kilometer: %.2f%n" +
                        "Vehicle type: %s%n" +
                        "Carts amount: %d%n",
                getPassengerCapacity(), getPricePerKilometer(), getType(), getCarts());
    }
}
