package com.telerikacademy.oop.agency.models;

import com.telerikacademy.oop.agency.models.contracts.Journey;
import com.telerikacademy.oop.agency.models.vehicles.VehicleType;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;

public class JourneyImpl implements Journey {

    private static final int MIN_LENGTH = 5;
    private static final int MAX_LENGTH = 25;
    private static final int MIN_DISTANCE = 5;
    private static final int MAX_DISTANCE = 5000;
    private static final String START_LOCATION_ERROR = String.format(
            "The StartingLocation's length cannot be less than %d or more than %d symbols long.",
            MIN_LENGTH, MAX_LENGTH);
    private static final String DESTINATION_ERROR = String.format(
            "The Destination's length cannot be less than %d or more than %d symbols long.",
            MIN_LENGTH, MAX_LENGTH);
    private static final String DISTANCE_ERROR = String.format(
            "The Distance cannot be less than %d or more than %d kilometers.",
            MIN_DISTANCE, MAX_DISTANCE);
    private String startLocation;
    private String destination;
    private int distance;
    private Vehicle vehicle;

    public JourneyImpl(String startLocation, String destination, int distance, Vehicle vehicle) {
        setStartLocation(startLocation);
        setDestination(destination);
        setDistance(distance);
        setVehicle(vehicle);
    }

    @Override
    public String getStartLocation() {
        return startLocation;
    }

    private void setStartLocation(String startLocation) {
        ValidationHelper.validateLocationAndDestination(startLocation, MIN_LENGTH, MAX_LENGTH, START_LOCATION_ERROR);

        this.startLocation = startLocation.trim();
    }

    @Override
    public String getDestination() {
        return destination;
    }

    private void setDestination(String destination) {
        ValidationHelper.validateLocationAndDestination(destination, MIN_LENGTH, MAX_LENGTH, DESTINATION_ERROR);

        this.destination = destination;
    }

    @Override
    public int getDistance() {
        return distance;
    }

    private void setDistance(int distance) {
        ValidationHelper.validateNumberInQuantity(distance, MIN_DISTANCE, MAX_DISTANCE, DISTANCE_ERROR);

        this.distance = distance;
    }

    @Override
    public Vehicle getVehicle() {
        return vehicle;
    }

    private void setVehicle(Vehicle vehicle) {
        if (vehicle == null) {
            throw new IllegalArgumentException("Vehicle cannot be null.");
        }
        this.vehicle = vehicle;
    }

    @Override
    public double calculateTravelCosts() {

        return  getDistance() * vehicle.getPricePerKilometer();
    }

    @Override
    public String toString() {
        return String.format("Journey ----%n" +
                        "Start location: %s%n" +
                        "Destination: %s%n" +
                        "Distance: %d%n" +
                        "Vehicle type: %s%n" +
                        "Travel costs: %.2f%n",
                getStartLocation(), getDestination(), getDistance(), getVehicle().getType(), calculateTravelCosts());
    }
}
