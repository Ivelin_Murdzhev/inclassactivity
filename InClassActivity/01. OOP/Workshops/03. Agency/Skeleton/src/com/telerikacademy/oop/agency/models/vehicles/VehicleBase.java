package com.telerikacademy.oop.agency.models.vehicles;

import com.telerikacademy.oop.agency.models.ValidationHelper;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleBase implements Vehicle {

    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType vehicleType;

    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType vehicleType) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        setVehicleType(vehicleType);
    }

    private void setPassengerCapacity(int passengerCapacity) {
        ValidationHelper.validateVehiclesCapacity(passengerCapacity);

        this.passengerCapacity = passengerCapacity;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        ValidationHelper.validatePricePerKilometer(pricePerKilometer);

        this.pricePerKilometer = pricePerKilometer;
    }

    private void setVehicleType(VehicleType vehicleType) {

        this.vehicleType = vehicleType;
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

}
