<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

# Unit Testing for Cosmetics Shop

## Description
You are given a software system for managing a cosmetics shop. The system is already implemented and works. It supports one type of products that can be grouped in categories. The user can:
- Create a category;
- Create a product;
- Add a product to a category;
- Show a category;

## Task
Your **task** is to cover the functionality with **unit tests**. You should cover all **models**, **commands** and the **CommandParserImpl** class.

### 1. Start with the models
- Category tests cases are defined.
- Continue with Product tests. First define the test cases.
- Target code coverage is 76%

### 2. Next test the commands
- CreateCategory test cases are defined.
- Consider using @BeforeEach to simplify your code.
- Continue with CreateProduct, AddProductToCategory and ShowCategory.
- Test cases are based on the functionality in the respective classes.
- Target code coverage is 90%

### 3. Test CommandParserImpl
- Test cases are defined.
- Target code coverage is 100%
