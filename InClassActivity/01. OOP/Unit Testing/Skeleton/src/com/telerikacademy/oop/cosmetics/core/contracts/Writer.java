package com.telerikacademy.oop.cosmetics.core.contracts;

public interface Writer {

    void write(String message);

    void writeLine(String message);
}
