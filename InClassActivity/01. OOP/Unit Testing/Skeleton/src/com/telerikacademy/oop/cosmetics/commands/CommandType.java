package com.telerikacademy.oop.cosmetics.commands;

public enum CommandType {
    CREATECATEGORY,
    CREATEPRODUCT,
    ADDPRODUCTTOCATEGORY,
    SHOWCATEGORY,
}
