package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.core.contracts.Category;
import com.telerikacademy.oop.cosmetics.core.contracts.Product;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;

import java.util.List;

public class AddProductToCategory implements Command {
    private static final int EXPECTED_PARAMETERS_COUNT = 2;
    private static final String INVALID_PARAMETERS_COUNT_MESSAGE = String.format(
            "AddProductToCategory command expects %d parameters.",
            EXPECTED_PARAMETERS_COUNT);

    private static final String CATEGORY_NOT_EXIST_MESSAGE = "Category %s does not exist.";
    private static final String PRODUCT_NOT_EXIST_MESSAGE = "Product %s does not exist.";
    private static final String PRODUCT_ADDED_TO_CATEGORY = "Product %s added to category %s!";

    private final ProductRepository productRepository;
    private String result;

    public AddProductToCategory(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void execute(List<String> parameters) {
        if (parameters.size() < EXPECTED_PARAMETERS_COUNT) {
            throw new InvalidUserInputException(INVALID_PARAMETERS_COUNT_MESSAGE);
        }

        String categoryNameToAdd = parameters.get(0);
        String productNameToAdd = parameters.get(1);

        result = addProductToCategory(categoryNameToAdd, productNameToAdd);
    }

    @Override
    public String getResult() {
        return result;
    }

    private String addProductToCategory(String categoryName, String productName) {
        if (!productRepository.getCategories().containsKey(categoryName)) {
            throw new InvalidUserInputException(String.format(CATEGORY_NOT_EXIST_MESSAGE, categoryName));
        }

        if (!productRepository.getProducts().containsKey(productName)) {
            throw new InvalidUserInputException(String.format(PRODUCT_NOT_EXIST_MESSAGE, productName));
        }

        Category category = productRepository.getCategories().get(categoryName);
        Product product = productRepository.getProducts().get(productName);

        category.addProduct(product);

        return String.format(PRODUCT_ADDED_TO_CATEGORY, productName, categoryName);
    }
}
