package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CategoryImplTests {

    private CategoryImpl category;
    private ProductImpl product;

    @BeforeEach
    public void before() {
        category = new CategoryImpl("Nivea");
        product = new ProductImpl("Test", "brand", 10, GenderType.MEN);

        category.addProduct(product);
        category.addProduct(product);
    }

    @Test
    public void constructor_Should_CreateCategory_When_ArgumentsAreValid() {

        //Assert
        Assertions.assertEquals(category.getName(), "Nivea");
    }

    @Test
    public void constructor_Should_ThrowException_When_NameIsInvalid() {
        //Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> new CategoryImpl("TestCAse1234"));

    }

    @Test
    public void addProduct_Should_AddProductToList() {

        //Assert
        Assertions.assertEquals(2, category.getProducts().size());

    }

    @Test
    public void removeProduct_Should_RemoveProductFromList_When_ProductExist() {

        //Act
        category.removeProduct(product);

        //Assert
        Assertions.assertEquals(1, category.getProducts().size());

    }

    @Test
    public void removeProduct_Should_NotRemoveProductFromList_When_ProductNotExist() {

        //Arrange
        ProductImpl productTwo = new ProductImpl("Test", "brand", 10, GenderType.MEN);

        //Act
        category.removeProduct(product);
        category.removeProduct(productTwo);

        //Assert
        Assertions.assertEquals(1, category.getProducts().size());

    }
}
