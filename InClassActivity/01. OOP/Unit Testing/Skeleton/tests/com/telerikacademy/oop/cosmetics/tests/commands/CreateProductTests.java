package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.CreateCategory;
import com.telerikacademy.oop.cosmetics.commands.CreateProduct;
import com.telerikacademy.oop.cosmetics.core.ProductFactoryImpl;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.Arrays.asList;

public class CreateProductTests {

    private Command commandTest;
    private ProductRepository productRepository;
    private ProductFactory productFactory;
    private List<String> params;

    @BeforeEach
    public void before() {
        //Arrange
        productRepository = new ProductRepositoryImpl();
        productFactory = new ProductFactoryImpl();
        commandTest = new CreateProduct(productRepository, productFactory);
        //Act
        commandTest.execute(asList("Nivea", "MyMan", "7", "Men"));

    }

    @Test
    public void execute_Should_AddNewCategoryToRepository_When_ValidParameters() {

        // Assert
        Assertions.assertEquals(1, productRepository.getProducts().size());
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                commandTest.execute(asList()));

    }

    @Test
    public void execute_Should_ThrowException_When_DuplicateCategoryName() {

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                commandTest.execute(asList("Nivea")));

    }

}
