package com.telerikacademy.oop.cosmetics.tests.commands;

import com.telerikacademy.oop.cosmetics.commands.CreateCategory;
import com.telerikacademy.oop.cosmetics.core.ProductFactoryImpl;
import com.telerikacademy.oop.cosmetics.core.ProductRepositoryImpl;
import com.telerikacademy.oop.cosmetics.core.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.ProductRepository;
import com.telerikacademy.oop.cosmetics.exceptions.DuplicateEntityException;
import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.CategoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.List;

public class CreateCategoryTests {

    private Command commandTest;
    private ProductRepository productRepository;
    private List<String> params;

    @BeforeEach
    public void before() {
        //Arrange
        productRepository = new ProductRepositoryImpl();
        ProductFactory productFactory = new ProductFactoryImpl();
        commandTest = new CreateCategory(productRepository, productFactory);
        //Act
        //commandTest.execute(asList("Test"));
        params = new ArrayList<>();
    }

    @Test
    public void execute_Should_AddNewCategoryToRepository_When_ValidParameters() {

        params.add("Test");
        commandTest.execute(params);

        Assertions.assertTrue(productRepository.getCategories().containsKey("Test"));
        // Assert
        //Assertions.assertEquals(1, productRepository.getCategories().size());
    }

    @Test
    public void execute_Should_ThrowException_When_MissingParameters() {

        Assertions.assertThrows(InvalidUserInputException.class, () ->
                commandTest.execute(params));
        // Act, Assert
        /*Assertions.assertThrows(InvalidUserInputException.class, () ->
                commandTest.execute(asList()));*/

    }

    @Test
    public void execute_Should_ThrowException_When_DuplicateCategoryName() {

        productRepository.getCategories().put("Name", new CategoryImpl("Name"));
        params.add("Name");

        Assertions.assertThrows(DuplicateEntityException.class, () ->
                commandTest.execute(params));
        // Act, Assert
        /*Assertions.assertThrows(DuplicateEntityException.class, () ->
                commandTest.execute(asList("Test")));*/
    }
}
