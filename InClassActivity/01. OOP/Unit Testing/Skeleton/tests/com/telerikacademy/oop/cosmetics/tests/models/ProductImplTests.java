package com.telerikacademy.oop.cosmetics.tests.models;

import com.telerikacademy.oop.cosmetics.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.cosmetics.models.GenderType;
import com.telerikacademy.oop.cosmetics.models.ProductImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ProductImplTests {

    @Test
    public void constructor_Should_CreateProduct_When_ArgumentsAreValid(){
        // Arrange, Act
        ProductImpl product = new ProductImpl("Nivea", "MyMan", 15, GenderType.MEN);

        // Assert
        Assertions.assertEquals(product.getName(), "Nivea");
        Assertions.assertEquals(product.getBrand(), "MyMan");
        Assertions.assertEquals(product.getPrice(), 15);
        Assertions.assertEquals(product.getGender(), GenderType.MEN);
    }

    @Test
    public void product_Should_ThrowException_When_NameIsSmaller_ThanMinValue() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                new ProductImpl("n", "brand", 10, GenderType.MEN));

    }

    @Test
    public void product_Should_ThrowException_When_NameIsLarger_ThanMaxValue() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                new ProductImpl("testNiveaImpact", "brand", 10, GenderType.MEN));
    }

    @Test
    public void product_Should_ThrowException_When_BrandIsSmaller_ThanMinValue() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                new ProductImpl("Test", "b", 10, GenderType.MEN));

    }

    @Test
    public void product_Should_ThrowException_When_BrandIsLarger_ThanMaxValue() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                new ProductImpl("Test", "testNiveaForMen", 10, GenderType.MEN));
    }

    @Test
    public void product_Should_ThrowException_When_PriceIsNegative() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () ->
                new ProductImpl("Nivea", "MyMan", -1, GenderType.MEN));
    }

}
