package com.telerikacademy;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("ForLoopReplaceableByForEach")
public class DataProcessor {

    public static int findTheAverageAgeOfPeopleWhoDislikeMovies(List<Customer> customers, Movie targetMovie) {

        /*var customersWhoDislikeMovie = new ArrayList<Customer>();

        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getDislikedMovies().contains(targetMovie)) {
                customersWhoDislikeMovie.add(customers.get(i));
            }
        }
        var sum = 0;
        for (int i = 0; i < customersWhoDislikeMovie.size(); i++) {
            sum += customersWhoDislikeMovie.get(i).getAge();
        }
        return sum / customersWhoDislikeMovie.size();*/
        return (int) customers.stream()
                .filter(customer -> customer.getDislikedMovies().contains(targetMovie))
                .mapToInt(Customer::getAge)
                .average()
                .getAsDouble();


    }

    public static int findHowManyPeopleLikeMove(List<Customer> customers, Movie targetMovie) {

        /*var count = 0;
        for (int i = 0; i < customers.size(); i++) {
            for (int j = 0; j < customers.get(i).getLikedMovies().size(); j++) {
                if (customers.get(i).getLikedMovies().get(j).equals(targetMovie)) {
                    count++;
                }
            }
        }*/
        return (int) customers.stream()
                .filter(customer -> customer.getLikedMovies()
                        .contains(targetMovie))
                .count();

    }

    public static int findAverageAgeOfAllCustomers(List<Customer> customers) {

        /*var sum = 0;
        for (int i = 0; i < customers.size(); i++) {
            sum += customers.get(i).getAge();
        }

        return sum / customers.size();*/
        return customers.stream()
                .mapToInt(Customer::getAge).sum() / customers.size();

    }

    public static boolean findIfAllCustomersDislikeMovie(List<Customer> customers, Movie targetMovie) {

        /*var result = true;
        for (int i = 0; i < customers.size(); i++) {
            if (!customers.get(i).getDislikedMovies().contains(targetMovie)) {
                result = false;
                break;
            }
        }*/
        return customers.stream().allMatch(customer -> customer.getDislikedMovies().contains(targetMovie));

    }

    public static ArrayList<Object> findAllCustomersAboveTargetAgeThatLikeGenre(List<Customer> customers, int targetAge, Genre targetGenre) {

        /*var result = new ArrayList<>();
        for (int i = 0; i < customers.size(); i++) {
            var customer = customers.get(i);
            if (customer.getAge() == targetAge) {
                for (int j = 0; j < customer.getLikedMovies().size(); j++) {
                    if (customer.getLikedMovies().get(j).getGenre().equals(targetGenre)) {
                        result.add(customer);
                    }
                }
            }
        }*/
        return customers.stream()
                .filter(customer -> customer.getAge() == targetAge)
                .filter(customer -> customer.getLikedMovies()
                        .stream()
                        .anyMatch(movie -> movie.getGenre().equals(targetGenre))).
                        collect(Collectors.toCollection(ArrayList::new));

    }

    public static int countCustomersAboveTargetAge(List<Customer> customers, int targetAge) {

        /*int result = 0;
        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getAge() > targetAge) {
                result++;
            }
        }

        return result;*/
        return (int) customers.stream()
                .filter(customer -> customer.getAge() > targetAge)
                .count();
    }

    public static boolean findIfAnyCustomersHasTargetName(List<Customer> customers, String targetName) {

        /*boolean result = false;
        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getName().equals(targetName)) {
                result = true;
                break;
            }
        }

        return result;*/
        return customers.stream()
                .anyMatch(customer -> customer.getName().equals(targetName));
    }

    public static boolean findIfAllCustomersAreAboveTargetAge(List<Customer> customers, int targetAge) {

        /*boolean result = true;
        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getAge() < targetAge) {
                result = false;
                break;
            }
        }

        return result;*/
        return customers.stream()
                .allMatch(customer -> customer.getAge() >= targetAge);
    }

    public static List<Customer> findAllCustomersUnderTargetAge(List<Customer> customers, int targetAge) {

        /*List<Customer> customersUnderTargetAge = new ArrayList<>();

        for (int i = 0; i < customers.size(); i++) {
            if (customers.get(i).getAge() < targetAge) {
                customersUnderTargetAge.add(customers.get(i));
            }
        }

        return customersUnderTargetAge;*/
        return customers.stream()
                .filter(c -> c.getAge() < targetAge)
                .collect(Collectors.toList());
    }

    public static Customer findTheCustomerWithTheLongestName(List<Customer> customers) {

        /*var withLongestName = customers.get(0);
        for (int i = 1; i < customers.size(); i++) {
            if (customers.get(i).getName().length() > withLongestName.getName().length()) {
                withLongestName = customers.get(i);
            }
        }
        return withLongestName;*/

        return customers.stream()
                .reduce(customers.get(0), (c1, c2) -> c1.getName().length() > c2.getName().length() ? c1 : c2);

    }

    public static List<Customer> findAllCustomersWhoLikeOnlyMoviesWithGenre(List<Customer> customers, Genre targetGenre) {

        /*var result = new ArrayList<Customer>();
        for (Customer customer : customers) {
            var customerMovies = customer.getLikedMovies();
            var areSameGenre = false;

            for (int j = 0; j < customerMovies.size() - 1; j++) {
                if (!customerMovies.get(j).getGenre().equals(targetGenre)) break;

                if (customerMovies.get(j).getGenre().equals(customerMovies.get(j + 1).getGenre())) {
                    areSameGenre = true;
                    break;
                }
            }

            if (areSameGenre) {
                result.add(customer);
            }
        }

        return result;*/
        return customers.stream().
                filter(customer -> customer.getLikedMovies()
                        .stream().allMatch(movie -> movie.getGenre().equals(targetGenre)))
                .collect(Collectors.toList());
    }
}
