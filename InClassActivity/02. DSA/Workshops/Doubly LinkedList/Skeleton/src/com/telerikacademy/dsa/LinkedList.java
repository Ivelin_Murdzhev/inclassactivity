package com.telerikacademy.dsa;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements List<T> {
    private Node head;
    private Node tail;
    private int size = 0;

    public LinkedList() {
    }

    public LinkedList(Iterable<T> iterable) {
        iterable.forEach(this::addLast);
    }

    @Override
    public void addFirst(T value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void addLast(T value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, T value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T getFirst() {

        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        return head.value;
    }

    @Override
    public T getLast() {

        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        return tail.value;
    }

    @Override
    public T get(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int indexOf(T value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T removeFirst() {

        if (isEmpty()) {
            throw new NoSuchElementException();
        }


        var old = head;
        head.prev = null;
        size--;

        return old.value;
    }

    @Override
    public T removeLast() {

        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        var old = tail;
        tail = tail.prev;

        if (tail == null) {
            head = null;
        }else{

        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<T> iterator() {
        throw new UnsupportedOperationException();
    }

    private class Node {
        T value;
        Node prev;
        Node next;

        Node(T value) {
            this.value = value;
        }
    }
}
