package com.telerikacademy.dsa;

import java.util.List;

public interface BinarySearchTree<E extends Comparable<E>> {

    E getRootValue();

    void setRootValue(E value);

    BinarySearchTreeImpl<E> getLeftTree();

    boolean hasLeftTree();

    void setLeftTree(BinarySearchTreeImpl<E> node);

    BinarySearchTreeImpl<E> getRightTree();

    boolean hasRightTree();

    void setRightTree(BinarySearchTreeImpl<E> node);

    void insert(E element);

    boolean search(E element);

    BinarySearchTreeImpl<E> find(E element);

    List<E> inOrder();

    List<E> postOrder();

    List<E> preOrder();

    List<E> bfs();

    int height();
    
    // Advanced task: implement remove method. To test, uncomment the commented tests in BinaryTreeImplTests
    boolean remove(E value);
}
