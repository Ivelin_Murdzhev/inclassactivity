package com.telerikacademy.dsa;

import java.util.*;

public class BinarySearchTreeImpl<E extends Comparable<E>> implements BinarySearchTree<E> {

    private E value;
    private BinarySearchTreeImpl<E> left;
    private BinarySearchTreeImpl<E> right;

    public BinarySearchTreeImpl(E value) {
        this.value = value;
        left = null;
        right = null;
    }

    @Override
    public E getRootValue() {
        return value;
    }

    @Override
    public void setRootValue(E value) {
        this.value = value;
    }

    @Override
    public BinarySearchTreeImpl<E> getLeftTree() {
        return left;
    }

    @Override
    public BinarySearchTreeImpl<E> getRightTree() {
        return right;
    }

    @Override
    public boolean hasLeftTree() {
        return this.getLeftTree() != null;
    }

    @Override
    public void setLeftTree(BinarySearchTreeImpl<E> node) {
        this.left = node;
    }

    @Override
    public boolean hasRightTree() {
        return this.getRightTree() != null;
    }

    @Override
    public void setRightTree(BinarySearchTreeImpl<E> node) {
        this.right = node;
    }

    @Override
    public BinarySearchTreeImpl<E> find(E element) {
        return find(element, this);
    }

    @Override
    public void insert(E value) {
        insertValue(value, this);
    }

    @Override
    public boolean search(E value) {
        return find(value, this) != null;
    }

    @Override
    public List<E> inOrder() {
        List<E> listOrder = new ArrayList<>();
        inOrder(this, listOrder);
        return listOrder;
    }

    @Override
    public List<E> postOrder() {
        return postOrder(this);
    }

    @Override
    public List<E> preOrder() {
        List<E> listOrder = new ArrayList<>();
        preOrder(this, listOrder);
        return listOrder;
    }

    @Override
    public List<E> bfs() {
        return bfs(this);
    }

    @Override
    public int height() {
        return getHeight();
    }

    @Override
    public boolean remove(E value) {
        return remove(value, this);
    }

    private boolean remove(E value, BinarySearchTreeImpl<E> root) {

        BinarySearchTreeImpl<E> nodeToRemove = find(value);
        if (nodeToRemove == null) return false;

        if (nodeToRemove.isLeaf()) {
            return removeLeaf(nodeToRemove, root);
        }

        BinarySearchTreeImpl<E> replacementNode;
        if (nodeToRemove.hasLeftTree()) {
            replacementNode = nodeToRemove.getLeftTree().getMaxValueNode();
        } else {
            replacementNode = nodeToRemove.getRightTree().getMinValueNode();
        }

        remove(replacementNode.getRootValue(), root);
        nodeToRemove.setRootValue(replacementNode.getRootValue());

        return true;
    }

    private boolean isLeaf() {

        return getLeftTree() == null && getRightTree() == null;
    }

    private boolean removeLeaf(BinarySearchTreeImpl<E> leaf,
                               BinarySearchTreeImpl<E> root) {

        BinarySearchTreeImpl<E> parent = getParent(leaf, root);
        if (parent == null) return false;

        if (parent.getLeftTree() == leaf) parent.setLeftTree(null);
        else parent.setRightTree(null);

        return true;
    }

    private BinarySearchTreeImpl<E> getParent(BinarySearchTreeImpl<E> target,
                                              BinarySearchTreeImpl<E> currentParent) {

        if (currentParent == null) return null;

        if (currentParent.getLeftTree() == target || currentParent.getRightTree() == target) {
            return currentParent;
        }
        if (target.getRootValue().compareTo(currentParent.getRootValue()) <= 0) {
            return getParent(target, currentParent.getLeftTree());
        }
        return getParent(target, currentParent.getRightTree());
    }

    private BinarySearchTreeImpl<E> getMaxValueNode() {

        if (right == null) return this;
        return right.getMaxValueNode();
    }

    private BinarySearchTreeImpl<E> getMinValueNode() {

        if (left == null) return this;
        return left.getMinValueNode();
    }

    private void insertValue(E value, BinarySearchTree<E> head) {

        if (value.compareTo(head.getRootValue()) <= 0) {
            if (head.getLeftTree() == null) {
                head.setLeftTree(new BinarySearchTreeImpl<>(value));
            } else {
                insertValue(value, head.getLeftTree());
            }
        } else {
            if (head.getRightTree() == null) {
                head.setRightTree(new BinarySearchTreeImpl<>(value));
            } else {
                insertValue(value, head.getRightTree());
            }
        }
    }

    private void preOrder(BinarySearchTreeImpl<E> node, List<E> result) {

        if (node != null) {
            result.add(node.getRootValue());
            preOrder(node.getLeftTree(), result);
            preOrder(node.getRightTree(), result);
        }
    }

    private List<E> postOrder(BinarySearchTreeImpl<E> node) {

        List<E> result = new ArrayList<>();
        if (node == null) return Collections.emptyList();

        result.addAll(postOrder(node.getLeftTree()));
        result.addAll(postOrder(node.getRightTree()));
        result.add(node.getRootValue());
        return result;
    }

    private void inOrder(BinarySearchTreeImpl<E> node, List<E> result) {

        if (node != null) {
            inOrder(node.getLeftTree(), result);
            result.add(node.getRootValue());
            inOrder(node.getRightTree(), result);
        }
    }

    private List<E> bfs(BinarySearchTreeImpl<E> head) {

        List<E> result = new ArrayList<>();
        Queue<BinarySearchTree<E>> queue = new LinkedList<>();
        if (head == null) return Collections.emptyList();

        queue.add(head);
        while (!queue.isEmpty()) {
            BinarySearchTree<E> current = queue.poll();
            result.add(current.getRootValue());
            if (current.getLeftTree() != null) {
                queue.offer(current.getLeftTree());
            }
            if (current.getRightTree() != null) {
                queue.offer(current.getRightTree());
            }
        }
        return result;
    }

    private BinarySearchTreeImpl<E> find(E value, BinarySearchTreeImpl<E> head) {

        if (head == null) return null;

        int comparison = value.compareTo(head.getRootValue());
        if (comparison == 0) return head;
        if (comparison < 0) return find(value, head.getLeftTree());

        return find(value, head.getRightTree());
    }

    private int getHeight() {

        int leftHeight = 0;
        int rightHeight = 0;

        if (left == null && right == null) {
            return 0;
        }
        if (getLeftTree() != null) {
            leftHeight = left.getHeight();
        }
        if (getRightTree() != null) {
            rightHeight = right.getHeight();
        }
        return 1 + Math.max(leftHeight, rightHeight);
    }
}
