package com.telerikacademy.dsa.stack;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class ArrayStack<E> implements Stack<E> {

    private E[] data;
    private int top;
    private int capacity = 10;

    public ArrayStack() {

        this.data = (E[]) new Object[capacity];
    }

    @Override
    public void push(E element) {

        addElements(element);
    }

    @Override
    public E pop() {
        if (!isEmpty()) {

            return data[--top];
        } else {
            throw new NoSuchElementException("Empty stack");
        }
    }

    @Override
    public E peek() {
        if (!isEmpty()) {

            return data[size() - 1];
        } else {
            throw new NoSuchElementException("Empty stack");
        }
    }

    @Override
    public int size() {

        return top;
    }

    @Override
    public boolean isEmpty() {

        return top == 0;
    }

    private void addElements(E element) {
        if (data.length == top) {
            E[] newArrayData = Arrays.copyOf(data, size() * 2);
        }

        data[size()] = element;
        top++;
    }

}
