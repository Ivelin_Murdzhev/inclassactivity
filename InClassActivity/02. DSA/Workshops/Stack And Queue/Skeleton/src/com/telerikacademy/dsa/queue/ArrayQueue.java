package com.telerikacademy.dsa.queue;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class ArrayQueue<E> implements Queue<E> {

    private E[] data;
    private int head, tail, size;

    public ArrayQueue() {
        data = (E[]) new Object[10];
        size = 0;
        head = 0;
        tail = -1;
    }

    @Override
    public void enqueue(E element) {
        if (data.length == tail) {
            data = Arrays.copyOf(data, data.length * 2 + 1);
        }

        data[++tail] = element;
        size++;
    }

    @Override
    public E dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        var element = data[head];
        data = Arrays.copyOfRange(data, 1, data.length);
        tail--;
        size--;
        return element;
    }

    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        return data[head];
    }

    @Override
    public int size() {

        return size;
    }

    @Override
    public boolean isEmpty() {

        return size == 0;
    }
}
