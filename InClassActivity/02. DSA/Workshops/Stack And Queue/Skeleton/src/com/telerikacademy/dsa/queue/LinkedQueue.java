package com.telerikacademy.dsa.queue;

import com.telerikacademy.dsa.Node;

import java.util.NoSuchElementException;

public class LinkedQueue<E> implements Queue<E> {

    private Node<E> head, tail;
    private int size;

    public LinkedQueue() {

        this.size = 0;
    }

    @Override
    public void enqueue(E element) {

        Node node = new Node(element);
        tail = node;

        if (isEmpty()) {
            head = node;
        } else {
            tail.next = node;
        }

        size++;
    }

    @Override
    public E dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        var element = head.data;
        head = head.next;
        size--;

        return element;
    }

    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }

        return head.data;
    }

    @Override
    public int size() {

        return size;
    }

    @Override
    public boolean isEmpty() {

        return size == 0;
    }
}
