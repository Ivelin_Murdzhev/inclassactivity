package com.telerikacademy.dsa.stack;

import com.telerikacademy.dsa.Node;

import java.util.NoSuchElementException;

public class LinkedStack<E> implements Stack<E> {

    private Node<E> top;
    private int size;

    @Override
    public void push(E element) {

        Node node = new Node(element);
        if (top != null) {

            node.next = top;
        }
        top = node;
        size++;
    }

    @Override
    public E pop() {
        if (top == null) {
            throw new NoSuchElementException("Empty stack");
        }

        E data = top.data;
        top = top.next;
        size--;

        return data;
    }

    @Override
    public E peek() {
        if (isEmpty()) {
            throw new NoSuchElementException("Empty stack");
        }

        return top.data;
    }

    @Override
    public int size() {

        return size;
    }

    @Override
    public boolean isEmpty() {

        return top == null;
    }
}
