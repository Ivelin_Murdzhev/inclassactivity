package com.telerikacademy.dsa;

public class Main {

    public static void main(String[] args) {

        writeHtml(getSampleNode(), 0);
    }

    public static void writeHtml(HtmlNode rootNode, int indent) {

        if (rootNode.getChildNodes().isEmpty()) {
            System.out.printf(" ".repeat(indent));
            rootNode.printOpenTag(indent);
            rootNode.printCloseTag();
            return;
        }

        System.out.printf(" ".repeat(indent));
        rootNode.printOpenTag(++indent);
        System.out.println();

        for (HtmlNode child : rootNode.getChildNodes()) {

            writeHtml(child, indent + 1);
        }

        System.out.printf(" ".repeat(indent - 1));
        rootNode.printCloseTag();
    }

    private static HtmlNode getSampleNode() {
        HtmlNode root = new HtmlNode("html");

        HtmlNode head = new HtmlNode("head");
        root.getChildNodes().add(head);

        HtmlNode title = new HtmlNode("title");
        head.getChildNodes().add(title);

        HtmlNode body = new HtmlNode("body");
        root.getChildNodes().add(body);
        body.getAttributes().add(new HtmlAttribute("class", "container"));

        HtmlNode div1 = new HtmlNode("div");
        body.getChildNodes().add(div1);
        div1.getAttributes().add(new HtmlAttribute("class", "navbar-container"));
        div1.getAttributes().add(new HtmlAttribute("id", "navbar"));

        HtmlNode div2 = new HtmlNode("div");
        body.getChildNodes().add(div2);
        div2.getAttributes().add(new HtmlAttribute("class", "main-container"));
        div2.getAttributes().add(new HtmlAttribute("id", "main-container"));

        return root;
    }

}
