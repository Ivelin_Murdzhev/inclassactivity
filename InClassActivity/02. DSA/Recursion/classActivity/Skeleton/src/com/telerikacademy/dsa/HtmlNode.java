package com.telerikacademy.dsa;

import java.util.ArrayList;
import java.util.List;

public class HtmlNode {

    private String name;
    private List<HtmlAttribute> attributes;
    private List<HtmlNode> childNodes;

    public HtmlNode(String name) {
        this.name = name;
        attributes = new ArrayList<>();
        childNodes = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HtmlAttribute> getAttributes() {
        return attributes;
    }

    public List<HtmlNode> getChildNodes() {
        return childNodes;
    }

    public void printOpenTag(int indent) {
        StringBuilder builder = new StringBuilder();
        if (indent > 0) {
            builder.append("<")
                    .append(getName());
            for (HtmlAttribute attribute : attributes) {
                builder.append(" ")
                        .append(attribute.getName())
                        .append("=")
                        .append("\"")
                        .append(attribute.getValue())
                        .append("\"");
            }
            builder.append(">");
        }

        System.out.print(builder.toString());
    }

    public void printCloseTag() {
        StringBuilder builder = new StringBuilder();

        builder.append("</")
                .append(getName())
                .append(">");

        System.out.println(builder.toString());
    }
}
