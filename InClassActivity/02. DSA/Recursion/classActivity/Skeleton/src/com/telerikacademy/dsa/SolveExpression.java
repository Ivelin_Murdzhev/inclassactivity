package com.telerikacademy.dsa;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SolveExpression {

    public static void main(String[] args) {

        System.out.println(solve("45+(55)"));
        System.out.println(solve("45+(24*(12+3))"));
        System.out.println(solve("12*(35-(46*(5+15)))"));
        System.out.println(solve("15/3"));
    }

    static int solve(String expression) {

        if (expression.startsWith("(") && expression.endsWith(")")) {
            expression = expression.substring(1, expression.length() - 1);
        }
        if (isDigit(expression)) {
            return Integer.parseInt(expression);
        }

        int position = getPosition(expression);
        String left = expression.substring(0, position);
        String right = expression.substring(position + 1);

        if (expression.charAt(position) == '+') {

            return addDigits(Integer.parseInt(left), solve(right));

        } else if (expression.charAt(position) == '-') {

            return extractDigit(Integer.parseInt(left), solve(right));

        } else if (expression.charAt(position) == '*') {

            return multiplyDigit(Integer.parseInt(left), solve(right));

        } else {

            return divisionDigit(Integer.parseInt(left), solve(right));
        }

    }

    static boolean isDigit(String expression) {
        return expression.matches("[0-9]+");
    }

    static int getPosition(String expression) {

        Pattern pattern = Pattern.compile("\\*|\\+|\\-");
        Matcher matcher = pattern.matcher(expression);
        int position = -1;
        if (matcher.find()) {
            position = matcher.start();
        }
        return position;
    }

    static int addDigits(int left, int right) {
        return left + right;
    }

    static int extractDigit(int left, int right) {
        return left - right;
    }

    static int multiplyDigit(int left, int right) {
        return left * right;
    }

    static int divisionDigit(int left, int right) {
        return left / right;
    }
}
