package com.telerikacademy.com.controllersdemo.services;

import com.telerikacademy.com.controllersdemo.models.Beer;
import com.telerikacademy.com.controllersdemo.models.User;

import java.util.List;
import java.util.Optional;

public interface BeerService {

    List<Beer> getAll();

    Beer getById(int id);

    Beer getByName(String name);

    List<Beer> filter(Optional<Integer> styleId);

    void create(Beer beer);

    void update(Beer beer, User user);

    void delete(int id, User user);
}
