package com.telerikacademy.com.controllersdemo.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class BeerDto {

    @NotNull(message = "Name cannot be empty")
    @Size(min = 2, max = 20, message = "Name must be between 2 and 20 symbols!")
    private String name;

    @Positive(message = "ABV must be positive!")
    private double abv;

    @Positive(message = "Style ID should be positive!")
    private int styleId;

    public BeerDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }
}
