package com.telerikacademy.com.controllersdemo.services;

import com.telerikacademy.com.controllersdemo.models.User;
import com.telerikacademy.com.controllersdemo.models.UserDto;
import com.telerikacademy.com.controllersdemo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserModelMapper {

    private final UserRepository repository;

    @Autowired
    public UserModelMapper(UserRepository repository) {
        this.repository = repository;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(userDto, user);
        return user;
    }

    public User fromDto(UserDto userDto, int id) {
        User user = repository.getById(id);
        dtoToObject(userDto, user);
        return user;
    }

    private void dtoToObject(UserDto userDto, User user) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
    }
}
