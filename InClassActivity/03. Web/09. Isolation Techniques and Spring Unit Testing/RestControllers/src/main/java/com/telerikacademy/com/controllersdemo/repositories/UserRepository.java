package com.telerikacademy.com.controllersdemo.repositories;

import com.telerikacademy.com.controllersdemo.models.User;

import java.util.List;

public interface UserRepository {

    List<User> getAll();

    User getById(int id);

    User getByEmail(String email);

    User getByUsername(String username);

    void create(User user);

    void update(User user);

    void delete(int id);
}
