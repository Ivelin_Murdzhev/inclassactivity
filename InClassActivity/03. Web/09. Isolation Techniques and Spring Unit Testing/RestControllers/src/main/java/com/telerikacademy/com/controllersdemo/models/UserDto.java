package com.telerikacademy.com.controllersdemo.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDto {

    @NotNull(message = "Name cannot be null!")
    @Size(min = 3, max = 20, message = "Name must be between 3 and 20 symbols!")
    private String firstName;

    @NotNull(message = "Name cannot be null!")
    @Size(min = 3, max = 20, message = "Name must be between 3 and 20 symbols!")
    private String lastName;

    @NotNull(message = "Email cannot be null!")
    private String email;

    public UserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
