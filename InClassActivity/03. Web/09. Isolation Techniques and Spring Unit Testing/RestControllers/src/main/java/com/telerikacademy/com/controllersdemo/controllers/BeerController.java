package com.telerikacademy.com.controllersdemo.controllers;

import com.telerikacademy.com.controllersdemo.exceptions.DuplicateEntityException;
import com.telerikacademy.com.controllersdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.com.controllersdemo.exceptions.UnauthorizedOperationException;
import com.telerikacademy.com.controllersdemo.models.Beer;
import com.telerikacademy.com.controllersdemo.models.BeerDto;
import com.telerikacademy.com.controllersdemo.models.RatingDto;
import com.telerikacademy.com.controllersdemo.models.User;
import com.telerikacademy.com.controllersdemo.services.BeerService;
import com.telerikacademy.com.controllersdemo.services.BeerModelMapper;
import com.telerikacademy.com.controllersdemo.services.RatingService;
import com.telerikacademy.com.controllersdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/beers")
public class BeerController {

    private BeerService service;
    private final BeerModelMapper modelMapper;
    private final RatingService ratingService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public BeerController(BeerService service, BeerModelMapper modelMapper,
                          RatingService ratingService, UserService userService,
                          AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.ratingService = ratingService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Beer> getAll() {
        return service.getAll();
    }

    @GetMapping("/filter")
    public List<Beer> filter(@RequestParam(required = false) Optional<Integer> styleId) {
        return service.filter(styleId);
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public Beer getByName(@RequestParam String name) {
        try {
            return service.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Beer create(@RequestHeader HttpHeaders headers, @Valid @RequestBody BeerDto beerDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Beer beer = modelMapper.fromDto(beerDto);
            beer.setCreatedBy(user);
            service.create(beer);
            return beer;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Beer update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id,
                       @Valid @RequestBody BeerDto beerDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);

            Beer beer = modelMapper.fromDto(beerDto, id);
            service.update(beer, user);
            return beer;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);

            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping("/{id}/rating")
    public void rate(@RequestHeader HttpHeaders headers,
                     @PathVariable int id,
                     @RequestBody RatingDto dto) {

        Beer beer = getById(id);
        User userToRate = authenticationHelper.tryGetUser(headers);
        User user;
        try {
            user = userService.getById(dto.getUserId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        ratingService.rate(beer, user, dto.getRating(), userToRate);
    }

    @GetMapping("/{id}/rating")
    public Double averageRating(@PathVariable int id) {
        Beer beer = getById(id);
        Double rating = ratingService.getAverageRating(beer);

        if (rating == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The given beer has no ratings yet!");
        }

        return rating;
    }
}
