package com.telerikacademy.com.controllersdemo.repositories;

import com.telerikacademy.com.controllersdemo.models.Beer;

import java.util.List;
import java.util.Optional;

public interface BeerRepository {

    List<Beer> getAll();

    Beer getById(int id);

    void create(Beer beer);

    void update(Beer beer);

    void delete(int id);

    Beer getByName(String name);

    List<Beer> filter(Optional<Integer> styleId);
}
