<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# REST Controllers - Create REST Controllers in Spring

In this activity we will get to know REST Controllers in Sprig by writing some of them. 
Before we start we need to understand the Spring Overview Demo as we are going to use it as a foundation.

## 1. Download and run the Spring Overview Demo

Check if the requests are working as expected.

## 2. Create User POJO class in models

User should have an **id**, a **name**, and an **email**.

These fields should be validated.

## 3. Create UserController class in controllers

Use annotations to mark the class as REST controller.
Routing for the class should be **/api/users**

## 4. CRUD Operations on users

Create methods and annotate them properly to handle all CRUD requests.
We should be able to: 
 - get all users
 - get user by id
 - create **valid** user
 - update user (name and email)
 - delete user by id
 
 All requests must return proper HTTP status.
 
