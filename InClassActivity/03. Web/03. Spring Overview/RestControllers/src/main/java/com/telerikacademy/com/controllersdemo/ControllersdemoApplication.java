package com.telerikacademy.com.controllersdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControllersdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ControllersdemoApplication.class, args);
    }

}
