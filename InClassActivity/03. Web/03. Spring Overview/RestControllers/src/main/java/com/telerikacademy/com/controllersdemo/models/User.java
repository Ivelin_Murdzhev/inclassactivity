package com.telerikacademy.com.controllersdemo.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class User {

    @Positive(message = "Id must be positive!")
    private int id;

    @NotNull(message = "Name cannot be null!")
    @Size(min = 3, max = 20, message = "Name must be between 3 and 20 symbols!")
    private String name;

    @NotNull(message = "Email cannot be null!")
    private String email;

    public User() {
    }

    public User(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
