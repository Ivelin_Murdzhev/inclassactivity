package com.telerikacademy.com.controllersdemo.controllers;

import com.telerikacademy.com.controllersdemo.models.User;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final List<User> users;

    public UserController() {
        users = new ArrayList<>();

        users.add(new User(1, "Ivan", "ivanivanov@mail.bg"));
        users.add(new User(2, "Dimitar", "dimitargenov@abv.bg"));
        users.add(new User(3, "Boyan", "boyandimitrov@gmail.bg"));
        users.add(new User(4, "Petya", "petyaivanova@hotmail.bg"));
        users.add(new User(5, "Elitsa", "elitsaivanova@gmail.bg"));
    }

    @GetMapping
    public List<User> getAll(@RequestParam(required = false) Optional<String> name,
                             @RequestParam(required = false) Optional<String> email) {
        return users.stream()
                .filter(user -> name.isEmpty() || user.getName().contains(name.get()))
                .filter(user -> email.isEmpty() || user.getEmail().contains(email.get()))
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable int id) {
        return getId(id);
    }

    @PostMapping
    public User createUser(@Valid @RequestBody User newUser) {
        users.add(newUser);
        return newUser;
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable int id, @Valid @RequestBody User user) {

        User userToUpdate = getId(id);
        userToUpdate.setName(user.getName());
        userToUpdate.setEmail(user.getEmail());

        return userToUpdate;
    }

    @DeleteMapping("/{id}")
    public User delete(@PathVariable int id) {

        User userToDelete = getId(id);
        users.remove(userToDelete);

        return userToDelete;
    }

    private User getId(int id) {
        return users.stream()
                .filter(user -> user.getId() == id)
                .findFirst()
                .orElseThrow(() -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND,
                        String.format("User with id {%d} cannot be found!", id)));
    }
}
