-- 1. Write a SQL query to find the average salary of employees who have
-- been hired before year 2000 incl. Round it down to the nearest integer.
select round(avg(salary)) as 'Average salary', hire_date as 'Hire date'
from employees
where year(hire_date) <= 2000;

-- ####################################################################################################### --
-- 2. Write a SQL query to find all town names that end with a vowel (a,o,u,e,i).
select distinct t.name as 'Town name'
from towns t
where lcase(name) like '%a' or lcase(name) like '%o' or
      lcase(name) like '%u' or lcase(name) like '%e' or
      lcase(name) like '%i' order by name;

select t.name as 'Town name'
from towns t
where t.name regexp '[aeiou]$'; -- regex

select name
from towns
where right(name, 1) in ('a','o','u','e','i');

-- ####################################################################################################### --
-- 3. Write a SQL query to find all town names that start with a vowel (a,o,u,e,i).
select distinct t.name as 'Town name'
from towns t
where lcase(name) like 'a%' or lcase(name) like 'o%' or
      lcase(name) like 'u%' or lcase(name) like 'e%' or
      lcase(name) like 'i%' order by name;

select t.name as 'Town name'
from towns t
where t.name regexp '^[aeiou]'; -- regex

select name
from towns
where left(name, 1) in ('a','o','u','e','i');

-- ####################################################################################################### --
-- 4. Write a SQL query that outputs the name and the length of the town with the longest name.
select name as 'Town name', length(name) as 'with longest name'
from towns
where length(name) = (select max(length(name)) from towns);

-- not valid --
select name as 'Town name', length(name) as 'with longest name'
from towns order by length(name) desc limit 2;

-- ####################################################################################################### --
-- 5. Write a SQL query that outputs the name and the length of the town with the shortest name.
select name as 'Town name', length(name) as 'with shortest name'
from towns
where length(name) = (select min(length(name)) from towns);

-- not valid --
select name as 'Town name', length(name) as 'with shortest name'
from towns  order by length(name) limit 1;

-- ####################################################################################################### --
-- 6. Write a SQL query to find all employees with even IDs.
select *
from employees where employee_id % 2 = 0;

-- ####################################################################################################### --
-- 7. Write a SQL query to find the names and salaries of the employees that take the minimal salary in the company.
select concat(first_name, ' ', last_name) as 'Employee name',
       department_id as 'Department', salary as 'Salary'
from employees
where salary = (select min(salary) from employees);

-- ####################################################################################################### --
-- 8. Write a SQL query to find the names and salaries of the employees that have
-- a salary that is up to 10% higher than the minimal salary for the company.
select concat(first_name, ' ', last_name) as 'Employee name', salary as 'Salary'
from employees
where  salary <= (select min(salary) * 1.1
from employees order by salary);

-- ####################################################################################################### --
-- 9. Write a SQL query to find the full name, salary and department of
-- the employees that take the minimal salary in their department.
select concat_ws(' ', e.first_name, e.middle_name, e.last_name) as 'Full Name',
       d.name as 'Department', e.salary as 'Salary'
from employees e
join departments d on d.department_id = e.department_id
join (
    select min(salary) as salary, department_id
    from employees
    group by department_id) t on t.department_id = e.department_id and t.salary = e.salary;

-- ####################################################################################################### --
-- 10. Write a SQL query to find the average salary in department #1.
select round(avg(e.salary), 2) as 'Average salary', e.department_id as 'Department ID'
from employees e
where e.department_id = '1';

-- ####################################################################################################### --

-- 11. Write a SQL query to find the average salary in the "Sales" department.
select round(avg(e.salary), 2) as 'Average salary in "Sales"'
from employees e
join departments d on d.department_id = e.department_id
where d.name = 'Sales';

-- ####################################################################################################### --
-- 12. Write a SQL query that outputs the number of employees in the "Sales" department.
select count(e.employee_id) as 'Number of employees in the "Sales"'
from employees e
join departments d on d.department_id = e.department_id
where d.name = 'Sales';

-- ####################################################################################################### --
-- 13. Write a SQL query that outputs the number of employees that have manager.
select count(*) as 'Employees that have manager'
from employees
where manager_id is not null;

-- ####################################################################################################### --
-- 14. Write a SQL query that outputs the number of employees that don't have manager.
select count(*) as 'Employees that don''t have manager'
from employees
where manager_id is null;

-- ####################################################################################################### --
-- 15. Write a SQL query to find all departments, along with the average salary for each of them.
select round(avg(salary),2) as 'Salary', d.name as 'Department'
from employees
join departments d on employees.department_id = d.department_id
group by d.name
order by  d.name;

-- class
select d.name, d.department_id, avg(salary)
from employees e join departments d on d.department_id = e.department_id
group by d.name, d.department_id;

-- ####################################################################################################### --
-- 16. Write a SQL query to find all projects that took less than 1 year (365 days) to complete.
select project_id as ID, name as Name, start_date, end_date
from projects
where datediff(end_date, start_date) < 365;

-- ####################################################################################################### --
-- 17. Write a SQL query that outputs the number for employees from each town, grouped by department.
-- For example how many people from Bellevue work in Sales. How many people from Calgary work in Marketing,
-- and so on...

select t.name as 'Town name', d.name as 'Department', count(employee_id) as employees
from employees
join departments d on d.department_id = employees.department_id
join addresses a on a.address_id = employees.address_id
join towns t on t.town_id = a.town_id
group by d.name,t.name
order by employees desc;

-- ####################################################################################################### --
-- 18. Write a SQL query that outputs the first and last name of all managers that have exactly 5 employees.

select e.employee_id as 'Manager ID',
       concat(e.first_name, ' ',e.last_name) as 'MangerName',
       count(e.employee_id) as 'Employees'
from employees e
join employees emp on emp.manager_id = e.employee_id
group by e.employee_id, e.first_name, e.last_name
having count(e.employee_id) = 5;

-- class
select m.first_name, m.last_name, count(e.employee_id)
from employees e
join employees m on e.manager_id = m.employee_id
group by m.employee_id, m.first_name, m.last_name
having count(e.employee_id) = 5;

-- ####################################################################################################### --
-- 19. Write a SQL query to find all employees along with their managers.
-- For employees that do not have manager display the value "(no manager)".
select concat(e.first_name, ' ', e.last_name) as 'Employee name',
       ifnull(concat(m.manager_id, ' ', m.last_name),'no manager') as 'Manager name'
from employees e
left join employees m on e.manager_id = m.employee_id;

-- ####################################################################################################### --
-- 20. Write a SQL query that outputs the names of all employees whose last name is exactly 5 characters long.
select  concat(e.first_name, ' ', e.last_name) as 'Employee'
from employees e where length(last_name) = 5;

-- ####################################################################################################### --
-- 21. Write a SQL query that outputs the current date and time in the following format
-- "`day.month.year hour:minutes:seconds:milliseconds`".
select date_format(current_timestamp(), '%e.%c.%Y %T:%f') as 'Current date and time';

select date_format(now(6),'%d.%c.%Y %H:%i:%S:%f') as 'Current Date and Time';

-- ####################################################################################################### --
-- 22. Write a SQL query to display the average employee salary by department and job title.
-- For example, the average salary in Engineering for Design Engineer is 32,700.
select round(avg(salary),2) as 'Average salary', d.name as 'Department', e.job_title as 'Job title'
from employees e
join departments d on e.department_id = d.department_id
group by d.name, job_title
order by  d.name;

-- class
select d.name, e.job_title, avg(salary)
from employees e
join departments d on d.department_id = e.department_id
group by d.name, e.job_title;

-- ####################################################################################################### --
-- 23. Write a SQL query that outputs the town with most employees.
select count(*) as most_employees, t.name as 'Town'
from employees
join addresses a on a.address_id = employees.address_id
join towns t on t.town_id = a.town_id
group by t.name
order by most_employees desc limit 1;

-- class
select t.name, count(*) as number
from employees e
join addresses a on e.address_id = a.address_id
join towns t on t.town_id = a.town_id
group by t.name
having number = (select count(*) as number
                 from employees e
                 join addresses a on e.address_id = a.address_id
                 join towns t on t.town_id = a.town_id
                 group by t.name
                 order by number desc
                 limit 1);

-- ####################################################################################################### --
-- 24. Write a SQL query that outputs the number of managers from each town.
select count(distinct e.employee_id) as 'Number of managers', t.name as 'Town'
from employees e
join employees e2 on e.employee_id = e2.manager_id
join addresses a on a.address_id = e.address_id
join towns t on t.town_id = a.town_id
group by t.name;

-- class
select t.name,count(*)
from employees
join addresses a on a.address_id = employees.address_id
join towns t on t.town_id = a.town_id
where employee_id in (select distinct manager_id
                      from employees
                      where manager_id is not null)
group by t.name;


-- ####################################################################################################### --

-- 25. Write a SQL query to find the manager who is in charge of the most employees and his average salary.

select concat(e.first_name, ' ', e.last_name) as 'Manager',
       count(e2.employee_id) as employees_number, round(avg (e2.salary), 2) as 'Average salary'
from employees e
join employees e2 on e.employee_id = e2.manager_id
group by e2.manager_id
order by employees_number desc limit 1;

-- class
select e.first_name, e.last_name, ppl, round(avg_salary, 0)
from employees e
join (select manager_id as m_id, count(*) as ppl, avg(salary) as avg_salary
      from employees
      group by manager_id) as mid on mid.m_id = e.employee_id
where ppl = (select count(*)
             from employees
             group by manager_id
             order by count(*) desc
             limit 1);

-- ####################################################################################################### --
-- 26. Write a SQL query that outputs the names of the employees who have worked on the most projects.

select max(total) as 'Most project' , employee
from( select count(distinct project_id) as total, concat(e.first_name, ' ', e.last_name) as employee
from employees_projects ep
join employees e on ep.employee_id = e.employee_id
group by employee order by total desc ) as result
group by  employee;

-- class
select concat(first_name, ' ',  last_name)
from employees
where employee_id in
      (
          select employeesWithMaxProjects.employee_id
          from
              (select employee_id, count(*) as projectsCount
               from employees_projects
               group by employee_id
               having  projectsCount =
                       (select max(projectsCountTable.projectsCount)
                        from
                            (select count(*) as projectsCount
                             from employees_projects
                             group by employee_id) as projectsCountTable
                       )
              ) as employeesWithMaxProjects
      )

-- ####################################################################################################### --