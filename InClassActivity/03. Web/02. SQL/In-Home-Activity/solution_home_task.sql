-- 1. Write a SQL query that outputs all information about all departments.
select *
from departments;

-- ####################################################################################################### --
-- 2. Write a SQL query that outputs all department names.
select d.name
from departments d;

-- ####################################################################################################### --
-- 3. Write a SQL query that outputs first and last name of each employee, along with their salary.
select e.first_name, e.last_name, e.salary
from employees e;

-- ####################################################################################################### --
-- 4. Write a SQL query that outputs the full name of each employee.
select concat_ws(' ',first_name, middle_name, last_name) as 'Employee name'
from employees;

-- ####################################################################################################### --
-- 5. Write a SQL query to generate an email addresses for each employee.
-- Consider that the email domain is telerikacademy.com.
-- For example, John Doe's email would be "John.Doe@telerikacademy.com".
-- The produced column should be named "Full Email Addresses".SELECT DISTINCT salary
select concat(first_name, '.', last_name, '@telerikacademy.com') as 'Email Address'
from employees;

-- ####################################################################################################### --
-- 6. Write a SQL query to find all the different employee salaries.
select distinct salary
from employees;

-- ####################################################################################################### --
-- 7. Write a SQL query that outputs all information about the employees whose job title is "Sales Representative".
select *
from employees
where job_title = 'Sales Representative';

-- ####################################################################################################### --
-- 8. Write an SQL query to find all employees who have a salary that is bigger than their manager's.
select concat(e.first_name, ' ', e.last_name) as 'Employee name', e.manager_id, round(e.salary, 2) as Salary
from employees e
join employees m on e.manager_id = m.employee_id
where e.salary > m.salary;

select  concat(e.first_name, ' ', e.last_name) as 'Employee name', round(e.salary, 2) as Salary,
       m.first_name as 'Manager name', round(m.salary, 2) as Manager_Salary
from employees e
join employees m on e.manager_id = m.employee_id
where m.salary < e.salary;

-- ####################################################################################################### --
-- 9. Write a SQL query to find the names of all employees whose first name starts with "SA".
select *
from employees
where first_name like 'Sa%';

-- ####################################################################################################### --
-- 10. Write a SQL query to find the names of all employees whose last name contains "ei".
select *
from employees
where employees.last_name like '%ei%';

-- ####################################################################################################### --
-- 11. Write a SQL query to find all employees whose salary is in the range [20000…30000].
select concat(first_name, ' ', last_name) as 'Employee name', round(salary, 2) as Salary
from employees
where salary between 20000 and 30000;

-- ####################################################################################################### --
-- 12. Write a SQL query to find the names of all employees whose salary is 25000, 14000, 12500 or 23600.
select concat(first_name, ' ', last_name) as 'Employee name', round(salary, 2) as Salary
from employees
where salary in (25000, 14000, 12500, 23600);

-- ####################################################################################################### --
-- 13. Write a SQL query to find all employees that do not have manager.
select concat(first_name, ' ', last_name) as 'Employee name'
from employees
where manager_id is null;

-- ####################################################################################################### --
-- 14. Write a SQL query to find the names of all employees who were hired before their managers.
select e.first_name, e.last_name, e.hire_date, m.first_name, m.hire_date
from employees e
join employees m on e.manager_id = m.employee_id
where m.hire_date > e.hire_date;

-- ####################################################################################################### --
-- 15. Write a SQL query to find all employees whose salary is more than 50000.
-- Order them in decreasing order, based on their salary.
select *
from employees
where salary> 50000
order by salary desc;

-- ####################################################################################################### --
-- 16. Write a SQL query to find the top 5 best paid employees.
select concat(first_name, ' ', last_name) as Employee, round(salary, 2) as Salary
from employees
order by salary desc limit 5;

-- ####################################################################################################### --
-- 17. Write a SQL query that outputs all employees along their address.
select e.first_name, e.last_name, e.salary, a.text, t.name
from employees e
join addresses a on a.address_id = e.address_id
join towns t on t.town_id = a.town_id;

-- ####################################################################################################### --
-- 18. Write a SQL query to find all employees whose middle name is the same as the first letter of their town.
select concat(e.first_name, ' ', e.middle_name, ' ', e.last_name) AS Employee, t.name as Town
from employees e
left join addresses as a on a.address_id = e.address_id
left join towns as t on t.town_id = a.town_id
where middle_name = left(t.name, 1);

-- ####################################################################################################### --
-- 19. Write a SQL that outputs all employees (first and last name) that have a manager,
-- along with their manager (first and last name).
select  concat(e.first_name, ' ',e.last_name ) as Employee_name,
        concat(m.first_name, ' ',m.last_name ) as Manager_name
from employees e
join employees m on e.manager_id = m.employee_id;

-- ####################################################################################################### --
-- 20. Write a SQL that outputs all employees that have a manager (first and last name),
-- along with their manager (first and last name) and the employee's address.
select concat(e.first_name, ' ', e.last_name) as Employee,
       concat(m.first_name, ' ', m.last_name) as Manager,
       t.name as Town, a.text as Address
from
employees e
join employees m on m.employee_id = e.manager_id
join addresses a on e.address_id = a.address_id
join towns t on a.town_id = t.town_id
where e.manager_id = m.employee_id and e.address_id = a.address_id and a.town_id = t.town_id;

-- ####################################################################################################### --
-- 21. Write a SQL query to find all departments and all town names in a single column.
select name from departments
union
select name from towns;

-- ####################################################################################################### --
-- 22. Write a SQL query to find all employees and their manager,
-- along with the employees that do not have manager. If they do not have a manager, output "n/a".
select concat(e.first_name, ' ', e.last_name) as Employee,
       concat(ifnull(m.first_name,'n/a'), ' ', ifnull(m.last_name, '')) as Manager
from employees e
left join employees m on m.employee_id = e.manager_id;

-- ####################################################################################################### --
-- 23. Write a SQL query to find the names of all employees from the departments "Sales" AND "Finance"
-- whose hire year is between 1995 and 2005.

select concat(e.first_name, ' ', e.last_name) as Employee, e.hire_date as 'Hire date', d.name as Department
from employees e
join departments d on d.department_id = e.department_id
where d.name in ('Sales', 'Finance') and (YEAR(e.hire_date)) between 1995 and 2005;

-- ####################################################################################################### --