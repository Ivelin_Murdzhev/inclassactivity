package com.telerikacademy.com.controllersdemo.repositories;

import com.telerikacademy.com.controllersdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.com.controllersdemo.models.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final List<User> users;

    public UserRepositoryImpl() {
        users = new ArrayList<>();

        users.add(new User(1, "Ivan", "Ivanovski", "ivanivanov@mail.bg"));
        users.add(new User(2, "Dimitar", "Genov", "dimitargenov@abv.bg"));
        users.add(new User(3, "Boyan","Dimitrov", "boyandimitrov@gmail.bg"));
        users.add(new User(4, "Petya","Ivanova", "petyaivanova@hotmail.bg"));
        users.add(new User(5, "Elitsa","Ivanova", "elitsaivanova@gmail.bg"));
    }

    @Override
    public List<User> getAll() {
        return users;
    }

    @Override
    public User getById(int id) {
        return users.stream()
                .filter(user -> user.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("User", id));
    }

    @Override
    public User getByEmail(String email) {
        return users.stream()
                .filter(user -> user.getEmail().equals(email))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("User", "email", email));
    }

    @Override
    public void create(User user) {
        users.add(user);
    }

    @Override
    public void update(User user) {
        User userToUpdate = getById(user.getId());
        userToUpdate.setFirstName(user.getFirstName());
        userToUpdate.setLastName(user.getLastName());
        userToUpdate.setEmail(user.getEmail());
    }

    @Override
    public void delete(int id) {
        User userToDelete = getById(id);
        users.remove(userToDelete);
    }
}