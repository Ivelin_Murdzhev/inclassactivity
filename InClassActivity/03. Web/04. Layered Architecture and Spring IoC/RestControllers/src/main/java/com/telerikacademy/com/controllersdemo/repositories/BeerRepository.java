package com.telerikacademy.com.controllersdemo.repositories;

import com.telerikacademy.com.controllersdemo.models.Beer;

public interface BeerRepository extends Repository<Beer>{

    Beer getByName(String name);
}
