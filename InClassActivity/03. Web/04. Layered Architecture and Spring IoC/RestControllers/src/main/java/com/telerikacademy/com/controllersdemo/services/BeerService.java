package com.telerikacademy.com.controllersdemo.services;

import com.telerikacademy.com.controllersdemo.models.Beer;

public interface BeerService extends Service<Beer> {

    Beer getByName(String name);
}
