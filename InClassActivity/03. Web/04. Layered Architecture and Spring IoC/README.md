<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# Layers & IoC - Add layers for User

In this activity we will get to apply the layers architecture in Sprig by separating our application into the appropriate layers.
Before we start we need to understand the Spring Overview In-Class Activity as this activity builds upon it.

## 1. Open the solution of the Spring Overview In-Class Activity

## 2. Create the UserRepository class

Right now, all of our data, regarding the users of the application is stored and managed in the `UserController` class. Move data access and management to its own layer. 

Apply Dependency Inversion where needed.

## 3. Create the UserService class

Isolate the business logic of the application in its own layer.

Apply Dependency Inversion where needed.