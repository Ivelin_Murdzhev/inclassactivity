package com.telerikacademy.com.controllersdemo.services;

import com.telerikacademy.com.controllersdemo.models.Beer;
import com.telerikacademy.com.controllersdemo.models.User;

public interface UserService extends Service<User> {

    User getByEMail(String email);

    void addToWishList(User user, Beer beer);
}
