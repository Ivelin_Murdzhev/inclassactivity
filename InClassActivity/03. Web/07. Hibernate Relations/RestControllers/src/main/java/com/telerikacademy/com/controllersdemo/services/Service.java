package com.telerikacademy.com.controllersdemo.services;

import java.util.List;

public interface Service<T> {

    List<T> getAll();

    T getById(int id);

    void create(T object);

    void update(T object);

    void delete(int id);
}
