package com.telerikacademy.com.controllersdemo.repositories;

import com.telerikacademy.com.controllersdemo.models.Beer;
import com.telerikacademy.com.controllersdemo.models.Rating;
import com.telerikacademy.com.controllersdemo.models.User;

public interface RatingRepository {

    void update(Rating rating);

    void create(Rating rating);

    Rating getByUserAndBeer(User user, Beer beer);

    Double getAverage(Beer beer);
}
