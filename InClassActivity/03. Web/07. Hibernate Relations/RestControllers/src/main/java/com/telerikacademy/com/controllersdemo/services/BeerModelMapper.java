package com.telerikacademy.com.controllersdemo.services;

import com.telerikacademy.com.controllersdemo.models.Beer;
import com.telerikacademy.com.controllersdemo.models.BeerDto;
import com.telerikacademy.com.controllersdemo.models.Style;
import com.telerikacademy.com.controllersdemo.repositories.BeerRepository;
import com.telerikacademy.com.controllersdemo.repositories.StyleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeerModelMapper {

    private final BeerRepository repository;

    private final StyleRepository styleRepository;

    @Autowired

    public BeerModelMapper(BeerRepository repository, StyleRepository styleRepository) {
        this.repository = repository;
        this.styleRepository = styleRepository;
    }

    public Beer fromDto(BeerDto beerDto) {
        Beer beer = new Beer();
        dtoToObject(beerDto, beer);
        return beer;
    }

    public Beer fromDto(BeerDto beerDto, int id) {
        Beer beer = repository.getById(id);
        dtoToObject(beerDto, beer);
        return beer;
    }

    private void dtoToObject(BeerDto beerDto, Beer beer) {
        Style style = styleRepository.getByID(beerDto.getStyleId());
        beer.setName(beerDto.getName());
        beer.setAbv(beerDto.getAbv());
        beer.setStyle(style);
    }
}
