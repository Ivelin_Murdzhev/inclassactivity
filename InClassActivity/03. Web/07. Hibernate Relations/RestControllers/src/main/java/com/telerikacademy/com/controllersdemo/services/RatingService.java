package com.telerikacademy.com.controllersdemo.services;

import com.telerikacademy.com.controllersdemo.models.Beer;
import com.telerikacademy.com.controllersdemo.models.User;

public interface RatingService {

    void rate(Beer beer, User user, double rating);

    Double getAverageRating(Beer beer);
}
