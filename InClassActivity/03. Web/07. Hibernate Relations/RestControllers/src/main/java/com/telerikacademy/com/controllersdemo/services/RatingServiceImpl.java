package com.telerikacademy.com.controllersdemo.services;

import com.telerikacademy.com.controllersdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.com.controllersdemo.models.Beer;
import com.telerikacademy.com.controllersdemo.models.Rating;
import com.telerikacademy.com.controllersdemo.models.User;
import com.telerikacademy.com.controllersdemo.repositories.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl implements RatingService {

    private final RatingRepository repository;

    @Autowired
    public RatingServiceImpl(RatingRepository repository) {
        this.repository = repository;
    }

    @Override
    public void rate(Beer beer, User user, double ratingValue) {
        Rating existingRecord;
        try {
            existingRecord = repository.getByUserAndBeer(user, beer);
            existingRecord.setRating(ratingValue);
            repository.update(existingRecord);
        } catch (EntityNotFoundException e) {
            var rating = new Rating(user, beer, ratingValue);
            repository.create(rating);
        }
    }

    @Override
    public Double getAverageRating(Beer beer) {
        return repository.getAverage(beer);
    }

}
