package com.telerikacademy.com.controllersdemo.controllers;

import com.telerikacademy.com.controllersdemo.exceptions.DuplicateEntityException;
import com.telerikacademy.com.controllersdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.com.controllersdemo.models.Beer;
import com.telerikacademy.com.controllersdemo.models.User;
import com.telerikacademy.com.controllersdemo.models.UserDto;
import com.telerikacademy.com.controllersdemo.services.BeerService;
import com.telerikacademy.com.controllersdemo.services.UserModelMapper;
import com.telerikacademy.com.controllersdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService service;
    private final UserModelMapper modelMapper;
    private final BeerService beerService;

    @Autowired
    public UserController(UserService service, UserModelMapper modelMapper, BeerService beerServic) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.beerService = beerServic;
    }

    @GetMapping
    public List<User> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/wish-list")
    public List<Beer> getWishList(@PathVariable int id) {
        return new ArrayList<>(getUserById(id).getWishList());
    }

    @PostMapping("/{userId}/add")
    public void addBeerToWishList(@PathVariable int userId,
                                  @RequestBody Map<String, Object> body) {
        var user = getUserById(userId);
        var beerId = (int) body.get("beerId");

        Beer beer;
        try {
            beer = beerService.getById(beerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        service.addToWishList(user, beer);
    }

    @GetMapping("/search")
    public User getByEMail(@RequestParam String email) {
        try {
            return service.getByEMail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public User createUser(@Valid @RequestBody UserDto newUser) {
        try {
            User user = modelMapper.fromDto(newUser);
            service.create(user);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable int id, @Valid @RequestBody UserDto newUser) {
        try {
            User user = modelMapper.fromDto(newUser, id);
            service.update(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
