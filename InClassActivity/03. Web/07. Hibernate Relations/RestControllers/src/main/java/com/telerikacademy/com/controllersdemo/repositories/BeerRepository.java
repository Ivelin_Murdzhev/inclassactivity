package com.telerikacademy.com.controllersdemo.repositories;

import com.telerikacademy.com.controllersdemo.models.Beer;

import java.util.List;
import java.util.Optional;

public interface BeerRepository extends Repository<Beer>{

    Beer getByName(String name);

    List<Beer> filter(Optional<Integer> styleId);
}
