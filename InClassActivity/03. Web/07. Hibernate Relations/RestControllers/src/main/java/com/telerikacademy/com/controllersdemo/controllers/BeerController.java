package com.telerikacademy.com.controllersdemo.controllers;

import com.telerikacademy.com.controllersdemo.exceptions.DuplicateEntityException;
import com.telerikacademy.com.controllersdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.com.controllersdemo.models.Beer;
import com.telerikacademy.com.controllersdemo.models.BeerDto;
import com.telerikacademy.com.controllersdemo.models.RatingDto;
import com.telerikacademy.com.controllersdemo.models.User;
import com.telerikacademy.com.controllersdemo.services.BeerService;
import com.telerikacademy.com.controllersdemo.services.BeerModelMapper;
import com.telerikacademy.com.controllersdemo.services.RatingService;
import com.telerikacademy.com.controllersdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/beers")
public class BeerController {

    private BeerService service;
    private final BeerModelMapper modelMapper;
    private final RatingService ratingService;
    private final UserService userService;

    @Autowired
    public BeerController(BeerService service, BeerModelMapper modelMapper,
                          RatingService ratingService, UserService userService) {
        this.service = service;
        this.modelMapper = modelMapper;
        this.ratingService = ratingService;
        this.userService = userService;
    }

    @GetMapping
    public List<Beer> getAll() {
        return service.getAll();
    }

    @GetMapping("/filter")
    public List<Beer> filter(@RequestParam(required = false) Optional<Integer> styleId) {
        return service.filter(styleId);
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public Beer getByName(@RequestParam String name) {
        try {
            return service.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Beer create(@Valid @RequestBody BeerDto beerDto) {
        try {
            Beer beer = modelMapper.fromDto(beerDto);
            service.create(beer);
            return beer;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable int id, @Valid @RequestBody BeerDto beerDto) {

        try {
            Beer beer = modelMapper.fromDto(beerDto, id);
            service.update(beer);
            return beer;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{id}/rating")
    public void rate(@PathVariable int id, @RequestBody RatingDto dto) {
        Beer beer = getById(id);
        User user;
        try {
            user = userService.getById(dto.getUserId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        ratingService.rate(beer, user, dto.getRating());
    }

    @GetMapping("/{id}/rating")
    public Double averageRating(@PathVariable int id) {
        Beer beer = getById(id);
        Double rating = ratingService.getAverageRating(beer);

        if (rating == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The given beer has no ratings yet!");
        }

        return rating;
    }
}
