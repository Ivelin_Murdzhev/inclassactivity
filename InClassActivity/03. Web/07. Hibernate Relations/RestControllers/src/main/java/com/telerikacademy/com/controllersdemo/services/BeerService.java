package com.telerikacademy.com.controllersdemo.services;

import com.telerikacademy.com.controllersdemo.models.Beer;

import java.util.List;
import java.util.Optional;

public interface BeerService extends Service<Beer> {

    Beer getByName(String name);

    List<Beer> filter(Optional<Integer> styleId);
}
