package com.telerikacademy.com.controllersdemo.repositories;

import com.telerikacademy.com.controllersdemo.models.Style;

public interface StyleRepository {

    Style getByID(int id);
}
