package com.telerikacademy.com.controllersdemo.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "beers_ratings")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "beer_id")
    private Beer beer;

    @Column(name = "rating")
    private double rating;

    public Rating() {
    }

    public Rating(User user, Beer beer, double rating) {
        setUser(user);
        setBeer(beer);
        setRating(rating);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rating rating1 = (Rating) o;
        return id == rating1.id &&
                Objects.equals(getUser(), rating1.getUser()) &&
                Objects.equals(getBeer(), rating1.getBeer());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, getUser(), getBeer(), getRating());
    }
}
