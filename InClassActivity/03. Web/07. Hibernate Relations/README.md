<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# Hibernate Relations - In-Class Activity

In this activity, we will add relations to the Hibernate Basics In-class activity solution.

Before we start we need to understand the previous in-class activity as this activity builds upon it.

## 1. Open your solution of the previous activity or the solution that we've uploaded

## 2. Implement adding a beer to a wishlist

Add a new endpoint (`POST /api/users/{id}/wish-list`) that accepts a user id and a beer, via body, and adds the beer to the user's wishlist. If the beer is already in the list, throw the appropriate exception.

Add another endpoint (`GET /api/users/{id}/wish-list`) to get all beers in a user's wishlist.


## 3. Implement rating a beer

Add a new endpoint (`POST /api/beers/{id}/rating`) that accepts a beer ID and a body with the rating value and the user that is rating (in a later lesson we will learn how to use authentication via headers to eliminate the need to pass the `userId` in the body):

```json
{
  "rating": 3,
  "userId": 1
}
```

Implementing this feature will require you to add a new table to the database - `beers_ratings` with columns `beer_id`, `user_id` and `rating`. If a user is trying to rate a beer that he has already rated, replace his previous rating.

Add another new endpoint (`GET /api/beers/{id}/rating`) that returns the average rating of the given beer.