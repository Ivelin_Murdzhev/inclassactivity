create table car_category
(
    car_category_id int auto_increment
        primary key,
    category_name   varchar(20) not null,
    constraint car_categories_name_uindex
        unique (category_name)
);

create table car
(
    car_id          int auto_increment
        primary key,
    brand           varchar(30) not null,
    production_year int         not null,
    color           varchar(20) not null,
    car_category_id int         not null,
    mileage         int         not null,
    tax_per_day     double      not null,
    constraint car_car_category_fk
        foreign key (car_category_id) references car_category (car_category_id)
);

create table city
(
    city_id   int auto_increment
        primary key,
    city_name varchar(20) not null
);

create table client_category
(
    client_category_id int auto_increment
        primary key,
    description        text        not null,
    name               varchar(20) null,
    constraint client_category_name_uindex
        unique (name)
);

create table client
(
    client_id          int auto_increment
        primary key,
    first_name         varchar(20) not null,
    last_name          varchar(20) not null,
    birth_date         date        not null,
    client_category_id int         not null,
    constraint client_client_category_fk
        foreign key (client_category_id) references client_category (client_category_id)
);

create table insurance
(
    id          int auto_increment
        primary key,
    description text not null
);

create table location
(
    location_id int auto_increment
        primary key,
    city_id     int  not null,
    address     text not null,
    constraint locations_cities_fk
        foreign key (city_id) references city (city_id)
);

create table penalty_type
(
    penalty_type_id int auto_increment
        primary key,
    price           double      not null,
    name            varchar(30) null,
    constraint penalty_type_name_uindex
        unique (name)
);

create table refueling_options
(
    refueling_options_id int auto_increment
        primary key,
    name                 varchar(20) not null,
    constraint refueling_options_name_uindex
        unique (name)
);

create table additional_options
(
    additional_options_id int auto_increment
        primary key,
    fuel_options_id       int        not null,
    early_drop_off        tinyint(1) not null,
    constraint additional_options_refueling_option_fk
        foreign key (fuel_options_id) references refueling_options (refueling_options_id)
);

create table rental_insurance
(
    rental_id    int auto_increment
        primary key,
    insurance_id int not null,
    constraint rental_insurance_fk
        foreign key (insurance_id) references insurance (id)
);

create table rental
(
    rental_id            int auto_increment
        primary key,
    client_id            int  not null,
    car_id               int  not null,
    pick_up_location_id  int  not null,
    drop_off_location_id int  not null,
    rental_insurance_id  int  not null,
    from_date            date not null,
    to_date              date not null,
    constraint rental_car_fk
        foreign key (car_id) references car (car_id),
    constraint rental_clients_fk
        foreign key (client_id) references client (client_id),
    constraint rental_location_drop_fk
        foreign key (drop_off_location_id) references location (location_id),
    constraint rental_location_fk
        foreign key (pick_up_location_id) references location (location_id),
    constraint rental_rental_insurance_fk
        foreign key (rental_insurance_id) references rental_insurance (rental_id)
);

create table penalty_charges
(
    charges_id      int auto_increment
        primary key,
    rental_id       int not null,
    penalty_type_id int not null,
    constraint penalty_charges_penalty_type_fk
        foreign key (penalty_type_id) references penalty_type (penalty_type_id),
    constraint penalty_charges_rental_fk
        foreign key (rental_id) references rental (rental_id)
);

create table rental_options
(
    rental_options_id int auto_increment
        primary key,
    rental_id         int not null,
    options_id        int not null,
    constraint rental_options_additional_options_fk
        foreign key (options_id) references additional_options (additional_options_id),
    constraint rental_options_rental_fk
        foreign key (rental_id) references rental (rental_id)
);

create table reservations
(
    reserve_id           int auto_increment
        primary key,
    client_id            int  not null,
    car_category_id      int  not null,
    pick_up_location_id  int  not null,
    drop_off_location_id int  not null,
    rental_insurance_id  int  not null,
    from_date            date not null,
    to_date              date not null,
    constraint reservations_car_category_fk
        foreign key (car_category_id) references car_category (car_category_id),
    constraint reservations_clients_fk
        foreign key (client_id) references client (client_id),
    constraint reservations_location_drop_fk
        foreign key (drop_off_location_id) references location (location_id),
    constraint reservations_location_fk
        foreign key (pick_up_location_id) references location (location_id),
    constraint reservations_rental_insurance_fk
        foreign key (rental_insurance_id) references rental_insurance (rental_id)
);

create table reservation_options
(
    reservation_options_id int auto_increment
        primary key,
    reservation_id         int not null,
    options_id             int null,
    constraint reservation_options_additional_options_additional_options_id_fk
        foreign key (options_id) references additional_options (additional_options_id),
    constraint reservation_options_reservations_fk
        foreign key (reservation_id) references reservations (reserve_id)
);


