package com.telerikacademy.com.controllersdemo.repositories;

import com.telerikacademy.com.controllersdemo.models.User;

import java.util.List;

public interface UserRepository extends Repository<User> {

    User getByEmail(String email);
}
