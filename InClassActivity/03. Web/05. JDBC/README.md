<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# JDBC - Implement JDBC repository for User

In this activity we will implement a JDBC repository for the User model.
Before we start we need to understand the Layers and Spring IoC in-class activity as this activity builds upon it.

## 1. Open the solution of the Layers and Spring IoC In-Class Activity

## 2. Create users table

Create a new table users that can store data for the users. Add sample data. Update the db scrupts to include the users table and data.

## 3. Create new UserRepository class

Create a new user repository class that uses JDBC to connect to the database. Implement all operations that are defined in the repository interface. Replace the old repository with the new one.

## 4. (Optional) Create generic repository

Create a base generic repository that has all the common code between the Beer and User repositories.

*Hint: Base repository can have abstract getters for the SQL queries and methods with generic parameters and generic result.*