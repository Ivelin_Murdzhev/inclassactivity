package com.telerikacademy.com.controllersdemo.repositories;

import java.util.List;

public interface Repository<Т> {

    List<Т> getAll();

    Т getById(int id);

    void create(Т object);

    void update(Т object);

    void delete(int id);
}
