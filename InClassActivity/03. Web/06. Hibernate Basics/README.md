<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# Hibernate Basics - Implement Hibernate repository for User

In this activity we will implement a Hibernate repository for the User model.
Before we start we need to understand the previous in-class activity as this activity builds upon it.

## 1. Open the solution of the JDBC In-Class Activity

## 2. Rework the UserRepository class

Rework the user repository class that uses JDBC to use Hibernate to connect to the database. Implement all operations that are defined in the repository interface.

## 3. Add DTO classes

Add DTO classes for Beer and User and use them in the create and update methods of the controllers.

## 4. (Optional) Create generic repository

Create a base generic repository that has all the common code between the Beer and User repositories.