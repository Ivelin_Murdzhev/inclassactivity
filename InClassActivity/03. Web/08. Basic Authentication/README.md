<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

# Basic Authentication - In-Class Activity

In this activity, we will add authentication to the endpoints that need to be protected.

Before we start we need to understand the previous in-class activity as this activity builds upon it.

## 1. Open your solution of the previous activity or the solution that we've uploaded

## 2. Modify the database

You will need to add two new tables to the database - `roles` and `users_roles`.

Create entities for the Role and modify the User class accordingly. 

Should you get stuck, refer to the demo for this session.

## 3. Add the `AuthenticationHelper` class

Try to implement it yourself and should you get stuck, refer to the demo for this session.

## 4. Add authentication to `delete()` and `update()` and set the beer's creator in `create()` in `BeerController`

Remember, only an Admin or the beer's own creator can modify it.

## 5. Change the `rate()` method in `BeerController` to authenticate via a username passed through a header