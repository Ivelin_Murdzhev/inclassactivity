package com.telerikacademy.com.controllersdemo.services;

import com.telerikacademy.com.controllersdemo.exceptions.DuplicateEntityException;
import com.telerikacademy.com.controllersdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.com.controllersdemo.exceptions.UnauthorizedOperationException;
import com.telerikacademy.com.controllersdemo.models.Beer;
import com.telerikacademy.com.controllersdemo.models.User;
import com.telerikacademy.com.controllersdemo.repositories.BeerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BeerServiceImpl implements BeerService {

    public static final String MODIFY_BEER_ERROR_MESSAGE = "Only creator or Admin can modify a beer!";
    private BeerRepository repository;

    @Autowired
    public BeerServiceImpl(BeerRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Beer> getAll() {
        return repository.getAll();
    }

    @Override
    public Beer getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Beer getByName(String name) {
        return repository.getByName(name);
    }

    @Override
    public List<Beer> filter(Optional<Integer> styleId) {
        return repository.filter(styleId);
    }

    @Override
    public void create(Beer beer) {
        boolean duplicateExists = true;

        try {
            repository.getByName(beer.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Beer", "name", beer.getName());
        }

        repository.create(beer);
    }

    @Override
    public void update(Beer beer, User user) {

        if (!beer.getCreatedBy().equals(user) && !user.isAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_BEER_ERROR_MESSAGE);
        }

        boolean duplicateExists = true;

        try {
            Beer existingBeer = repository.getByName(beer.getName());
            if (existingBeer.getId() == beer.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Beer","name", beer.getName());
        }

        repository.update(beer);
    }

    @Override
    public void delete(int id, User user) {
        Beer beer = repository.getById(id);
        if (!beer.getCreatedBy().equals(user) && !user.isAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_BEER_ERROR_MESSAGE);
        }
        repository.delete(id);
    }
}
