package com.telerikacademy.com.controllersdemo.models;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

public class RatingDto {

    @Positive(message = "User ID should be positive!")
    private int userId;

    @Min(0)
    @Max(5)
    private double rating;

    public RatingDto() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
