package com.telerikacademy.com.controllersdemo.services;

import com.telerikacademy.com.controllersdemo.models.Beer;
import com.telerikacademy.com.controllersdemo.models.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getById(int id);

    User getByEMail(String email);

    User getByUsername(String username);

    void addToWishList(User user, Beer beer);

    void create(User user);

    void update(User user);

    void delete(int id);
}
