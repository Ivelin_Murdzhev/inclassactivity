package com.telerikacademy.com.controllersdemo.services;

import com.telerikacademy.com.controllersdemo.exceptions.DuplicateEntityException;
import com.telerikacademy.com.controllersdemo.exceptions.EntityNotFoundException;
import com.telerikacademy.com.controllersdemo.models.Beer;
import com.telerikacademy.com.controllersdemo.models.User;
import com.telerikacademy.com.controllersdemo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.repository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return repository.getAll();
    }

    @Override
    public User getById(int id) {
        return repository.getById(id);
    }

    @Override
    public User getByEMail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public void addToWishList(User user, Beer beer) {
        Set<Beer> newWishList = user.getWishList();
        if (newWishList.contains(beer)) {
            throw new DuplicateEntityException(
                    String.format("Beer with id %d is already in the wish list", beer.getId()));
        }
        newWishList.add(beer);
        user.setWishList(newWishList);
        repository.update(user);
    }

    @Override
    public User getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public void create(User user) {
        boolean duplicateExists = true;
        try {
            repository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        repository.create(user);
    }

    @Override
    public void update(User user) {
        boolean duplicateExists = true;
        try {
            User existingUser = repository.getByEmail(user.getEmail());
            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        repository.update(user);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }

}
